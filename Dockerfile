FROM node:8-alpine

ADD package.json /tmp/package.json

ADD package-lock.json /tmp/package-lock.json

RUN cd /tmp && npm install

RUN mkdir -p /app && cp -a /tmp/node_modules /app

WORKDIR /app

ADD . /app

RUN npm run build

FROM nginx:alpine-perl

COPY --from=0 /app/dist /var/www

COPY nginx.conf /etc/nginx

CMD ["nginx", "-g", "daemon off;"]
