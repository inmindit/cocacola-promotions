import React from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'helpers';

const NoData = ({
    hasData,
    textKey,
    children,
}) => (
    !hasData ? '' : children
);

NoData.defaultProps = {
    textKey: 'no-data',
};

NoData.propTypes = {
    hasData: PropTypes.bool.isRequired,
    textKey: PropTypes.string,
};

export default NoData;
