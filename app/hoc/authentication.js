import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { parse, stringify } from "qs";

import { getAccessToken, setAccessToken } from "actions/user";
import { Loader } from 'components/common';
import { getItemFromLocalStorage } from "helpers";

class AuthenticationProvider extends Component {
    componentDidMount() {
        this.authenticate();
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.isAccessTokenLoaded && this.props.isAccessTokenLoaded) {
           this.redirectToActiveUrl();
        }
    }

    authenticate = () => {
        const accessToken = this.getAccessToken();
        const { location } = this.props;
        const search = parse(location.search, {
            ignoreQueryPrefix: true
        });

        if (!accessToken && !search.code) {
            const redirectQuery = {
                scope: 'all',
                redirect_uri: `${window.location.origin}/login/oauth`,
                response_type: 'code',
                client_id: 'cce-coke-agent',
                ...search,
            };

            window.location.assign(`/login-redirect?${stringify(redirectQuery)}`);
        } else if (accessToken) {
            this.props.actions.setAccessToken(accessToken);
        } else {
            this.props.actions.getAccessToken(search.code);
        }
    };

    redirectToActiveUrl = () => {
        const { location } = this.props;
        const { customerId } = parse(location.search, { ignoreQueryPrefix: true });
        const url = customerId ? location.search : '/';

        this.props.history.replace(url);
    }

    getAccessToken = () => {
        const { accessToken } = parse(location.search, {
            ignoreQueryPrefix: true
        });
        return accessToken || getItemFromLocalStorage("COKE_AGENT.accessToken");
    };

    render() {
        const { isAccessTokenLoaded } = this.props;

        if (!isAccessTokenLoaded) {
            return <Loader />;
        }

        return (
            <Fragment>
                {this.props.children}
            </Fragment>
        );
    }
}

export default withRouter(
    connect(
        ({ user }) => ({
            accessToken: user.accessToken,
            isAccessTokenLoaded: user.isAccessTokenLoaded
        }),
        dispatch => ({
            actions: bindActionCreators(
                { getAccessToken, setAccessToken },
                dispatch
            )
        })
    )(AuthenticationProvider)
);
