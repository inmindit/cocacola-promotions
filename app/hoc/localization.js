import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import { Loader } from 'components/common';
import Strings, { loadLanguage } from 'helpers/strings';
import { setInitialLoaderText } from 'helpers';

class LocalizationProvider extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            loadedLangs: [],
            isLoading: false,
        };
    }

    componentDidMount() {
        this.checkLocalization();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.lang !== this.props.lang) {
            this.checkLocalization();
        }
    }

    checkLocalization() {
        if (this.state.isLoading) {
            return;
        }

        if (!this.hasLang()) {
            // Change the loading text
            setInitialLoaderText('loading.localization');

            this.setState({ isLoading: true });

            loadLanguage(this.props.lang).then(() => {
                this.setState({
                    loadedLangs: [...this.state.loadedLangs, this.props.lang],
                    isLoading: false,
                });
            });
        } else {
            Strings.setLanguage(this.props.lang);

            this.setState({ isLoading: true }, () => this.setState({ isLoading: false }));
        }
    }

    hasLang() {
        return this.state.loadedLangs.indexOf(this.props.lang) !== -1;
    }

    render() {
        if (this.state.isLoading) {
            return <Loader loadingTextKey="loading.localization" />;
        }

        return (
            <Fragment>
                { this.hasLang() ? this.props.children : null }
            </Fragment>
        );
    }
}

export default connect(
    state => ({
        lang: state.application.lang,
    }),
    null,
)(LocalizationProvider);
