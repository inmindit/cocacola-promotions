import axios from 'axios';
import LocalizedStrings from 'react-localization';
import Constants from './constants';

const languages = {
    de: {
        loading: 'Loading',
        'loading.app': 'Loading app',
        'loading.localization': 'Loading texts',
    },
};

const strings = new LocalizedStrings({ ...languages });

strings.setLanguage(Constants.DefaultSettings.lang);

export default strings;

export const loadLanguage = lang => new Promise((resolve, reject) => {
    axios.get(`/l10n/${lang}.json`).then(locales => {
        languages[lang] = {
            ...locales.data,
        };

        strings.setContent({ ...languages });

        strings.setLanguage(lang);

        resolve();
    });
});
