import Constants from './constants';
import Strings from './strings';

export { default as Constants } from './constants';
export { default as Strings, loadLanguage } from './strings';

export const getPlatformType = () => {
    const { clientWidth } = document.documentElement;
    const { MOBILE, TABLET, DESKTOP } = Constants.PlatformTypes;

    if (clientWidth < 500) {
        return MOBILE;
    } else if (clientWidth < 1000) {
        return TABLET;
    }

    return DESKTOP;
};

export const reducer = (initialState, reducers) =>
    (state = initialState, action = {}) =>
        (reducers[action.type] ? reducers[action.type](state, action) : state);

export const destroyInitialLoader = () => {
    const loader = document.getElementById('initial-loader');

    if (loader) {
        loader.parentNode.removeChild(loader);
    }
};

export const setInitialLoaderText = key => {
    const loaderText = document.getElementById('initial-loader-text');

    if (loaderText) {
        loaderText.innerHTML = Strings[key];
    }
};


export const getItemFromLocalStorage = key => localStorage.getItem(key);

export const setItemInLocalStorage = (key, value) => localStorage.setItem(key, value);

// const sumFormatter = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR', minimumFractionDigits: 2 });

const sumFormatterZero = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR', minimumFractionDigits: 0 });

export const formatSum = sum => {
    if (!sum) {
        return '';
    }
    if (+sum === 0) {
        return sumFormatterZero.format(+sum);
    }
    return sumFormatterZero.format(+sum);
};
