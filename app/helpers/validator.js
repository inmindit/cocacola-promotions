const validators = {
    required: value => !!value && !!String(value).trim(),
    regex: (value, regex) => new RegExp(regex).test(value),
    minLength: (value, length) => value && value.length >= length,
};

const validateField = (value, { rules, priorities = [], hints }) => {
    let error = '';

    priorities.every(rule => {
        const isValid = validators[rule](value, rules[rule]);

        if (!isValid) {
            error = hints[rule];
        }

        return isValid;
    });

    return error;
};

export const validate = ({ fields, fieldsConfig }) => {
    const errors = {};

    Object.keys(fields).forEach(key => {
        const field = fieldsConfig[key];
        errors[key] = validateField(fields[key], field);
    });

    const isValid = !Object.values(errors).filter(error => !!error).length;

    return { errors, isValid };
};
