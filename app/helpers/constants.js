const PlatformTypes = {
    DESKTOP: 'DESKTOP',
    MOBILE: 'MOBILE',
    TABLET: 'TABLET',
};

const DefaultSettings = {
    lang: 'de',
};

export const Routes = {
    main: '/',
    customerDetails: '/details',
    selectProduct: '/products',
    statistics: '/statistics',
    selectCustomer: '/select-customer',
    budget: '/budget',
    toolDetails: '/tool-details',
};

export default {
    PlatformTypes,
    DefaultSettings,
};
