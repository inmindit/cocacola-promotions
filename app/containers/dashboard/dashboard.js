import React, { Component } from 'react';
import moment from 'moment';
import { parse } from 'qs';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import { Button, Card, Carousel, Loader, Text } from 'components/common';

import { BudgetCardNew, BudgetModal, InvestmentDetails, Investments, Message, Section } from 'components';
import { setItemInLocalStorage, Strings } from 'helpers';
import { getProductData, getProducts } from 'actions/products';
import { updateToolsFilter } from 'actions/tools';
import { getCustomerData } from 'actions/customers';
import { getBudgetData, updateSelectedBudget } from 'actions/budget';
import { getUserWelcomeMessage } from 'actions/user';
import { updateWelcomeMessageCloseTimestamp, setLanguage } from 'actions/application';
import { SelectCustomer } from 'containers';
import ProductsAndTools from 'components/products-and-tools/products-and-tools';

import './dashboard.css';
import cokeLogoMessage from '../../../assets/cokeLogoMessage.jpg';

const CURRENT_YEAR_PERIOD = 2021;
const NEXT_YEAR_PERIOD = CURRENT_YEAR_PERIOD + 1;

const BUDGETS_ORDER = [
    'Outletaktivierung',
    'Digital',
    'Incentives'
];

class Dashboard extends Component {
    state = {
        showCustomerInfo: false,
        showProductsModal: false,
        showInvestmentDetails: false,
        showBudgetModal: false,
        showMDDNotification: false,
        showMDDNotificationError: false,
        serviceOrderNumber: '',
        period: CURRENT_YEAR_PERIOD,
        investmentDetailsSubBudgetName: 'Alle',
    };

    componentDidMount() {
        const {
            location,
            actions: { getCustomerData, getProductData, getBudgetData, getProducts, getUserWelcomeMessage },
        } = this.props;

        const { customerId, brands } = parse(location.search, { ignoreQueryPrefix: true });

        const {
            service,
            serviceSuccessful,
            serviceOrderNumber
        } = parse(location.search, { ignoreQueryPrefix: true });
        console.log(service, serviceSuccessful, serviceOrderNumber);

        if (service === 'mdd' && !!serviceSuccessful && !!serviceOrderNumber){
            if (serviceSuccessful === 'true') {
                this.setState({
                    showMDDNotification: true,
                    showMDDNotificationSuccess: true,
                    serviceOrderNumber: serviceOrderNumber || '',
                });
            } else if (serviceSuccessful === 'false') {
                this.setState({
                    showMDDNotification: true,
                    showMDDNotificationSuccess: false,
                    serviceOrderNumber: serviceOrderNumber || '',
                });
            }
        }

        getUserWelcomeMessage();
        getProducts();
        if (customerId) {
            getCustomerData(customerId);
            getBudgetData(customerId);
        }

        if(brands) {
            getProductData(brands);
        }
    }

    componentDidUpdate(prevProps) {
        const { activeView } = this.props;
        const { customerId: prevCustomerId } = parse(prevProps.location.search, { ignoreQueryPrefix: true });
        const { customerId } = parse(this.props.location.search, { ignoreQueryPrefix: true });

        if (customerId !== prevCustomerId) {
            this.props.actions.getCustomerData(customerId);
            if(customerId !== undefined){
                this.props.actions.getBudgetData(customerId);
            }
            if (this.state.period === 'all'){
                this.setState({ period: this.state.period = CURRENT_YEAR_PERIOD });
            }
        }

        if (prevProps.activeView.key === 'normal' && activeView.key === 'reporting' || prevProps.activeView.key === 'reporting' && activeView.key === 'normal') {
            this.props.actions.updateToolsFilter(activeView.key);
        }
    }

    toggleCustomerDetails = () => {
        this.setState({ showCustomerInfo: !this.state.showCustomerInfo });
    }

    onCardClicked = id => {
        this.props.actions.updateSelectedBudget(id);
        this.setState({ showBudgetModal: !this.state.showBudgetModal });
    }

    onBudgetDetailsClicked = () => {
        this.setState({ showInvestmentDetails: !this.state.showInvestmentDetails });
    }

    handleSubBudgetClick = name => {
        this.setState({
            showInvestmentDetails: true,
            investmentDetailsSubBudgetName: name
        });
    }

    getFilteredInvestments = () => {
        const { selectedSubBudget } = this.props;
        return selectedSubBudget.flatMap(subBudget =>
            subBudget.services.flatMap(service =>
                service.investments.filter(investment =>
                    investment.dateTime > CURRENT_YEAR_PERIOD.toString() && investment.dateTime < NEXT_YEAR_PERIOD.toString()).map(
                    investment => ({
                        ...investment,
                        serviceName: service.title,
                    })
                )
            )
        );
    }

    closeMessage() {
        this.props.actions.updateWelcomeMessageCloseTimestamp();
    }

    setProductPeriod(period) {
        this.setState({
            period
        });
    }

    handleInvestmentDetailsSubBudgetChange = subBudgetName => {
        if(subBudgetName !== undefined) {
            this.setState({
                investmentDetailsSubBudgetName: subBudgetName,
            });
        }
    }

    sortBudgets(budgets) {
        return budgets.sort((a, b) => {
            return BUDGETS_ORDER.indexOf(a.name) - BUDGETS_ORDER.indexOf(b.name);
        });
    }

    handleCloseMMDMessage = () => {
        this.setState({ showMDDNotification: false });
    }

    render() {
        const { isLoading, activeView, selectedCustomer, selectedSubBudget, budgets, welcomeMessage, lang } = this.props;
        const { showInvestmentDetails, showBudgetModal, period, showCustomerInfo } = this.state;
        const sortedBudgets = this.sortBudgets(budgets);
        const subBudgets = this.props.budgets !== undefined ? this.props.budgets.flatMap(budget => budget.subBudgetCategories).map(subBudget => {
            const services = subBudget.services.map(
                service => {
                    let investments = service.investments;

                    if (period !== 'all') {
                        investments = investments.filter(
                            investment => {
                                const investmentYear = Number(moment(investment.dateTime).format('YYYY'));

                                return investmentYear === period;
                            }
                        );
                    }

                    return {
                        ...service,
                        investments,
                    };
                }
            );

            const amount = services.reduce(
                (subBudgetTotalAmount, service) => subBudgetTotalAmount += service.investments.reduce(
                    (serviceTotalAmount, investment) => serviceTotalAmount += investment.amount,
                    0,
                ),
                0
            );

            return {
                ...subBudget,
                services,
                amount,
            };
        }) : [];

        if (isLoading) {
            return <Loader />;
        }

        return (
            <Card className="dashboard" contentClassName="dashboard-content" isLoading={false}>
                <div className="first-row">
                    <SelectCustomer
                        activeView={activeView}
                        onCustomerNameClick={this.toggleCustomerDetails}
                        showCustomerInfo={showCustomerInfo}
                        onCustomerInfoModalClose={this.toggleCustomerDetails}
                    />
                    { showBudgetModal && <BudgetModal investments={this.getFilteredInvestments()} selectedBudget={selectedSubBudget} onClose={this.onCardClicked}/>}
                    {
                        showInvestmentDetails && (
                            <InvestmentDetails
                                subBudgetName={this.state.investmentDetailsSubBudgetName}
                                subBudgets={
                                    subBudgets.filter(
                                        subBudget => {
                                            const investments = subBudget.services.flatMap(
                                                service => service.investments,
                                            );

                                            return investments.length > 0;
                                        },
                                    )
                                }
                                onClose={this.onBudgetDetailsClicked}
                                onSubBudgetChange={this.handleInvestmentDetailsSubBudgetChange}
                                period={this.state.period}
                            />
                        )
                    }
                </div>
                { selectedCustomer.sapNumber &&
                    <div>
                        { new Date(+this.props.welcomeMessageCloseTimestamp) < new Date(welcomeMessage.lastUpdatedAt) &&
                            <Message onClose={this.closeMessage.bind(this)} message={welcomeMessage.message} />
                        }

                    {this.state.showMDDNotification && !this.state.showMDDNotificationSuccess && (
                        <Message onClose={() => this.handleCloseMMDMessage()}
                                 message={(Strings['page.investmentDetails.MMDMessageError'])}
                                 imageURL={cokeLogoMessage}
                                 theme={'primary'}
                                 isError={!this.state.showMDDNotificationSuccess}
                        />
                    )}

                    {this.state.showMDDNotification && this.state.showMDDNotificationSuccess  && (
                        <Message onClose={() => this.handleCloseMMDMessage()}
                                 message={(Strings['page.investmentDetails.MMDMessage'].replace('$serviceOrderNumber', this.state.serviceOrderNumber))}
                                 imageURL={cokeLogoMessage}
                                 theme={'primary'}
                                 isError={!this.state.showMDDNotificationSuccess}
                        />
                    )}

                    <div className="third-row">
                        {sortedBudgets.length > 0 ? (
                                    <Section headerTextKey="customer.budget" hasDropdown={true} >
                                        {/* child 0 is always visible */}
                                        <Carousel
                                            dots={false}
                                            infinite={false}
                                            speed={500}
                                            slidesToShow={3}
                                            slidesToScroll={1}
                                        >
                                            {sortedBudgets.map(budget => (
                                                <BudgetCardNew
                                                    key={budget.identifier}
                                                    id={budget.identifier}
                                                    title={budget.name}
                                                    budgetUsed={budget.budgetUsed}
                                                    budgetLimit={budget.budgetLimit}
                                                    onClick={() => this.onCardClicked(budget.identifier)}
                                                />
                                            ))}
                                        </Carousel>

                                        <Card className="investments-filter" contentClassName="investments-filter-content">
                                            {/* child 1+ is hidden/expandable */}
                                            <Text type={Text.types.PRIMARY} size={Text.sizes.BIG} transform={Text.transform.UPPERCASE} >
                                                {Strings['budget.new.total-invest']}
                                            </Text>
                                            <div className="investments-filter__buttons-container">
                                                <Button.Contained
                                                    className={`section-all ${ this.state.period !== 'all' && '--unactive'}`}
                                                    textKey="budget.investments.filter.all"
                                                    onClick={() => this.setProductPeriod('all')}
                                                />
                                                <Button.Contained
                                                    className={`section-annual ${ this.state.period !== CURRENT_YEAR_PERIOD && '--unactive'}`}
                                                    textString={CURRENT_YEAR_PERIOD.toString()}
                                                    onClick={() => this.setProductPeriod(CURRENT_YEAR_PERIOD)}
                                                />
                                            </div>
                                            <Investments
                                                subBudgets={subBudgets}
                                                period={this.state.period}
                                                onSubBudgetClick={this.handleSubBudgetClick}
                                            />
                                        </Card>
                                    </Section>
                            ) : (
                                <p className="alert-no-budgets">
                                    {Strings['page.investmentDetails.noBudgetsFound']}
                                </p>
                            )
                        }
                        <ProductsAndTools />
                        </div>
                    </div>
                }
            </Card>
        );
    }
}

export default withRouter(
    connect(
        ({ products, customers, application, budget, user }) => ({
            isLoading: customers.isLoadingCustomerData || budget.isLoading,
            selectedCustomer: customers.selectedCustomer,
            activeView: application.activeView,
            products: products.products,
            user,
            welcomeMessageCloseTimestamp: application.welcomeMessageCloseTimestamp,
            budgets: budget.budgetData,
            selectedSubBudget: budget.selectedSubBudget,
            welcomeMessage: user.welcomeMessage,
            lang: application.lang,
        }),
        dispatch => ({
            actions: bindActionCreators(
                {
                    getCustomerData,
                    updateToolsFilter,
                    updateSelectedBudget,
                    getProductData,
                    getBudgetData,
                    getProducts,
                    getUserWelcomeMessage,
                    updateWelcomeMessageCloseTimestamp,
                    setLanguage,
                },
                dispatch,
            ),
        }),
    )(Dashboard),
);
