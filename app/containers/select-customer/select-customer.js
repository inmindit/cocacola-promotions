import React, { Fragment, PureComponent } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';

import { Button, Icon } from 'components/common';
import { SelectedCustomer, CustomerInfoModal } from 'components';
import { updateSelectedProduct } from 'actions/products';
import { setActiveView } from 'actions/application';
import { Routes } from 'constants';
import { BudgetDetails } from 'containers';

import CustomersModal from './customers-modal';

import './select-customer.css';

class SelectCustomer extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            showSelectCustomerModal: false,
            showBudgetDetails: props.activeView.key === 'budget',
        };
    }

    clearSelectedCustomer = () => {
        this.props.actions.updateSelectedProduct(null);
        this.props.history.replace({ pathname: Routes.main, search: '' });
    };

    toggleCustomersModal = () => {
        const activeView = this.props.activeToolsFilter

        this.setState({ showSelectCustomerModal: !this.state.showSelectCustomerModal });
        this.props.actions.setActiveView(activeView);
    };

    toggleBudgetDetails = () => {
        const activeView = this.state.showBudgetDetails ? this.props.activeToolsFilter : 'budget'

        this.setState({ showBudgetDetails: !this.state.showBudgetDetails });
        this.props.actions.setActiveView(activeView);
    };

    render() {
        const { showSelectCustomerModal, showBudgetDetails } = this.state;
        const { selectedCustomer, budget, onCustomerInfoModalClose, showCustomerInfo } = this.props;

        return (
            <Fragment>
                {selectedCustomer.sapNumber ? (
                    <SelectedCustomer
                        {...selectedCustomer}
                        selectedCustomer={selectedCustomer}
                        onCustomerInfoModalClose={onCustomerInfoModalClose}
                        showCustomerInfo={showCustomerInfo}
                        customerId={selectedCustomer.sapNumber}
                        name={selectedCustomer.name1}
                        budget={budget}
                        className="selected-customer"
                        onDeleteSelectedCustomerClick={this.clearSelectedCustomer}
                        onCustomerNameClick={this.props.onCustomerNameClick}
                        onBudgetClick={this.toggleBudgetDetails}
                    />
                ) : (
                    <Button.Contained
                        className="dashboard-button"
                        textKey="button.select-customer"
                        leftIcon={<Icon size="small" name="Add" />}
                        onClick={this.toggleCustomersModal}
                    />
                )}

                {showSelectCustomerModal && <CustomersModal onClose={this.toggleCustomersModal} />}
                {showBudgetDetails && <BudgetDetails onClose={this.toggleBudgetDetails} />}
            </Fragment>
        );
    }
}

export default withRouter(
    connect(
        ({ customers, tools, budget }) => ({
            selectedCustomer: customers.selectedCustomer,
            activeToolsFilter: tools.activeFilter,
            budget: budget.customerBudget
        }),
        dispatch => ({
            actions: bindActionCreators({ updateSelectedProduct, setActiveView }, dispatch),
        }),
    )(SelectCustomer),
);
