import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { parse, stringify } from "qs";
import { updateWelcomeMessageCloseTimestamp } from 'actions/application';
import { CustomersService } from "services";
import { Button, Modal } from "components/common";
import { CustomerInfo, CustomersList } from "components";

class CustomersModal extends Component {
    state = {
        isLoading: false,
        customers: []
    };

    componentDidMount() {
        this.fetchCustomers();
    }

    componentDidUpdate(prevProps, prevState) {
        const prevQuery = parse(prevProps.location.search, {
            ignoreQueryPrefix: true
        });
        const query = parse(this.props.location.search, {
            ignoreQueryPrefix: true
        });

        if (prevQuery.customerSearch !== query.customerSearch) {
            this.fetchCustomers();
        }

        if (!prevState.customers.length && this.state.customers.length) {
            this.props.history.replace({
                search: stringify({
                    ...query,
                    selectedCustomerId: this.state.customers[0].sapNumber
                })
            });
        }
    }

    updateQuery(update) {
        const query = parse(this.props.location.search, {
            ignoreQueryPrefix: true
        });

        this.props.history.replace({
            search: stringify({ ...query, ...update })
        });
    }

    updateSearch = customerSearch => {
        this.updateQuery({ customerSearch });
    };

    updateSelectedCustomer = selectedCustomerId => {
        this.updateQuery({ selectedCustomerId });
    };

    selectCustomer = () => {
        const query = parse(this.props.location.search, {
            ignoreQueryPrefix: true
        });

        this.props.history.replace({
            pathname: "/",
            search: stringify({ customerId: query.selectedCustomerId })
        });

        this.props.onClose();
    };

    fetchCustomers() {
        const query = parse(this.props.location.search, {
            ignoreQueryPrefix: true
        });

        const params = {
            query: query.customerSearch,
        };

        this.setState({ isLoading: true });

        CustomersService.getCustomers({ params }).then(customers => {
            const customerData = customers.find(({ sapNumber }) => sapNumber === query.selectedCustomerId);

            this.setState({ customers, isLoading: false });

            if (!customerData && customers.length) {
                this.updateSelectedCustomer(customers[0].sapNumber);
            }
        });
    }

    renderModalButtons() {
        return (
            <Modal.Buttons>
                <Button.Close onClick={this.props.onClose} />
                <Button.Choose onClick={this.selectCustomer} />
            </Modal.Buttons>
        );
    }
    render() {
        const { isLoading, customers } = this.state;
        const { selectedCustomerId, customerSearch } = parse(this.props.location.search, { ignoreQueryPrefix: true });
        const customerData = customers.find(({ sapNumber }) => sapNumber === selectedCustomerId);

        return (
            <Modal
                className="large-content"
                titleKey="modal.title.select-customer"
                closeIconLink="/"
                buttons={this.renderModalButtons()}
                onClose={this.props.onClose}
            >

                <div className="select-customer">
                    <CustomersList
                        isLoading={isLoading}
                        customers={customers}
                        search={customerSearch}
                        selectedCustomerId={selectedCustomerId}
                        onSearchChange={this.updateSearch}
                        onCustomerClick={this.updateSelectedCustomer}
                    />
                    <CustomerInfo
                        data={customerData}
                        hasData={!!customerData}
                        isLoading={isLoading}
                    />
                </div>
            </Modal>
        );
    }
}

export default withRouter(connect(
    ({ application,}) => ({
    }),
    dispatch => ({
        actions: bindActionCreators({
            updateWelcomeMessageCloseTimestamp,
        }, dispatch),
    }),
)(CustomersModal));
