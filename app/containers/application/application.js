import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import { setActiveView, setPlatformType } from 'actions/application';
import { getUserData, logout } from 'actions/user';

import { Navigation, ProfileMenu } from 'components';
import { Strings, getPlatformType, destroyInitialLoader } from 'helpers';

import { Routes as RoutesNames } from 'constants';


import './application.css';

const navigationConfig = {
    headerText: 'navItems.menu.title',
    headerIconName: 'Menu',
    items: [
        {
            key: 'normal',
            labelKey: 'navItems.dashboard.title',
            icon: 'Dashboard',
        },
    ],
};
class Application extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigationExpanded: false,
        };
    }

    componentDidMount() {
        destroyInitialLoader();

        window.addEventListener('resize', this.handleResize, false);

        this.props.actions.getUserData();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }

    handleResize = () => {
        const { platformType } = this.props;
        const currentPlatformType = getPlatformType();

        if (currentPlatformType !== platformType) {
            this.props.actions.setPlatformType(currentPlatformType);
        }
    }

    componentDidUpdate() {
        const { location } = this.props;
        (location.pathname === RoutesNames.newsPage) ? this.props.actions.setActiveView('news') : null
    }

    toggleExpand = () => this.setState({ navigationExpanded: !this.state.navigationExpanded });

    render() {
        const {
            children, userData, activeView, platformType, showNavigation,
        } = this.props;
        const { navigationExpanded } = this.state;

        return (
            <div className={`layout ${platformType.toLowerCase()} ${showNavigation ? '' : 'navigation-hidden'}`}>
                {showNavigation && (
                    <Navigation
                        {...navigationConfig}
                        selected={activeView}
                        expanded={navigationExpanded}
                        toggleExpand={this.toggleExpand}
                        onItemClick={this.props.actions.setActiveView}
                        userData={this.props.userData}
                        onLogoutClick={this.props.actions.logout}
                    />
                )}

                <div className="layout-content">
                    {children}
                </div>
            </div>
        );
    }
}

export default withRouter(connect(
    ({ application, user }) => ({
        platformType: application.platformType,
        activeView: application.activeView,
        showNavigation: application.showNavigation,
        userData: user.userData,
    }),
    dispatch => ({
        actions: bindActionCreators({
            setActiveView, getUserData, logout, setPlatformType,
        }, dispatch),
    }),
)(Application));
