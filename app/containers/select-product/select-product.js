import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import { getProducts, updateSelectedProduct } from 'actions/products';
import { setActiveView } from 'actions/application';
import { Button, Modal, Icon, Loader } from 'components/common';
import { ProductSelector as SelectProduct, ProductDetails } from 'components';

import './select-product.css';

class ProductSelector extends Component {
    state = {
        showProductsModal: false,
    };

    componentDidMount() {
        this.props.actions.getProducts();
    }

    updateSelectedProduct = id => {
        this.props.actions.updateSelectedProduct(id);
        this.toggleProductsModal();
    };

    mapProducts = () => {
        const { selectedProduct, products } = this.props;

        return products.map(product => (
            <SelectProduct
                {...product}
                key={product.id}
                className="product-item"
                selected={selectedProduct && selectedProduct.id === product.id}
                onClick={this.updateSelectedProduct}
            />
        ));
    };

    toggleProductsModal = () => {
        const activeView = this.state.showProductsModal ? this.props.activeToolsFilter : 'products';

        this.props.actions.setActiveView(activeView);
        this.setState({ showProductsModal: !this.state.showProductsModal });
    };

    clearSelectedProduct = () => this.props.actions.updateSelectedProduct(null);

    getModalButtons = () => (
        <Modal.Buttons>
            <Button.Close onClick={this.toggleProductsModal} />
        </Modal.Buttons>
    );

    render() {
        const { isLoadingProducts, selectedProduct, selectedCustomer } = this.props;
        const { showProductsModal } = this.state;

        let productsModalContent = '';

        if (showProductsModal) {
            productsModalContent = <Loader loadingTextKey="loading.products" />;

            if (!isLoadingProducts) {
                productsModalContent = <div className="products-container">{this.mapProducts()}</div>;
            }
        }

        return (
            <Fragment>
                {selectedCustomer.sapNumber && !selectedProduct.id && (
                    <Button.Contained
                        className="dashboard-button"
                        textKey="button.select-product"
                        leftIcon={<Icon size="small" name="Add" />}
                        onClick={this.toggleProductsModal}
                    />
                )}

                {selectedCustomer.sapNumber && selectedProduct.id && (
                    <ProductDetails {...selectedProduct} onDeleteProductClick={this.clearSelectedProduct} />
                )}

                {showProductsModal && (
                    <Modal titleKey="select" buttons={this.getModalButtons()} onClose={this.toggleProductsModal}>
                        {productsModalContent}
                    </Modal>
                )}
            </Fragment>
        );
    }
}

export default withRouter(
    connect(
        ({ products, customers, tools }) => ({
            products: products.products,
            isLoadingProducts: products.isLoading,
            selectedProduct: products.selectedProduct,
            selectedCustomer: customers.selectedCustomer,
            activeToolsFilter: tools.activeFilter
        }),
        dispatch => ({
            actions: bindActionCreators({ getProducts, updateSelectedProduct, setActiveView }, dispatch),
        }),
    )(ProductSelector),
);
