import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { Strings } from 'helpers';
import { setActiveView } from 'actions/application';
import { Card } from 'components/common';
import { ServicesCard } from 'components';
import { getTools, updateSelectedTool } from 'actions/tools';

import './tools.css';

class Tools extends Component {
    componentDidMount() {
        const {
            selectedCustomer,
            selectedProduct,
            userData,
            activeToolsFilter,
            actions: { getTools },
        } = this.props;

        getTools({
            selectedCustomer, selectedProduct, userData, filter: activeToolsFilter,
        });
    }

    componentDidUpdate(prevProps) {
        const {
            selectedCustomer,
            selectedProduct,
            userData,
            // activeView,
            actions: { getTools },
            activeToolsFilter,
        } = this.props;

        if (
            prevProps.selectedCustomer.sapNumber !== this.props.selectedCustomer.sapNumber ||
            prevProps.selectedProduct.id !== this.props.selectedProduct.id ||
            prevProps.activeToolsFilter !== activeToolsFilter
        ) {
            getTools({
                selectedCustomer, selectedProduct, userData, filter: activeToolsFilter,
            });
        }
    }

    render() {
        const { tools, isLoading } = this.props;
        const activeView = this.props.activeToolsFilter;
        const toolsServices = [];

        tools.forEach(t => t.services.forEach(s => toolsServices.push({ ...s, toolIdentifier: t.identifier })));

        toolsServices.sort((a, b) => +a.rank - +b.rank);

        return (
            <div>
                <Card isLoading={isLoading} className="tools-wrapper" contentClassName="tools-container">
                    {toolsServices.map(service => (
                        <ServicesCard
                            key={service.identifier}
                            title={service.budgetTitle}
                            secondTitle={service.title}
                            icon={service.icon}
                            textContent={service.description}
                            onHelpClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                this.props.onToolClick(service.toolIdentifier);
                                this.props.actions.setActiveView(activeView);
                            }}
                            onImageClick={() => {
                                window.location.assign(Strings.formatString(service.url, { sapNumber: this.props.selectedCustomer.sapNumber || '', brands: this.props.selectedProduct.id || '' }));
                            }}
                        />
                    ))
                    }
                </Card>
            </div>
        );
    }
}

export default withRouter(connect(
    ({
        application,
        products,
        customers,
        tools,
        user,
    }) => ({
        selectedCustomer: customers.selectedCustomer,
        selectedProduct: products.selectedProduct,
        isLoading: tools.isLoading,
        tools: tools.tools,
        selectedTool: tools.selectedTool,
        userData: user.userData,
        activeToolsFilter: tools.activeFilter,
        platformType: application.platformType,
    }),
    dispatch => ({
        actions: bindActionCreators({ getTools, updateSelectedTool, setActiveView }, dispatch),
    }),
)(Tools));
