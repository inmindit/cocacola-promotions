import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import { Application, Dashboard, Gate, SelectCustomer } from 'containers';
import { Routes as RoutesNames } from 'constants';

const Router = () => (
    <BrowserRouter>
        <Gate>
            <Switch>
                <Application>
                    <Route exact path={RoutesNames.main} component={Dashboard} />
                    <Route exact path={RoutesNames.selectCustomer} component={SelectCustomer} />
                </Application>

                <Redirect to={RoutesNames.main} />
            </Switch>
        </Gate>
    </BrowserRouter>
);

export default Router;
