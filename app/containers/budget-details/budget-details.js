import React, { Component } from 'react';
import { connect } from 'react-redux';
import { findIndex } from 'lodash';
import { BudgetCard, InvestmentsList } from 'components';
import { Strings } from 'helpers';
import { Text, Modal, Filter } from 'components/common';

import './budget-details.css';
import { InvertColorsRounded } from '@material-ui/icons';

class BudgetDetails extends Component {
    state = {
        investments: [],
        filteredInvestments: [],
        selectedBudgetIndex: 0,
        currentYear: new Date().getFullYear(),
        selectedFilter: 'currentYear',
    };

    componentDidMount() {
        /* Temporary until we have the unique identifiers */
        this.updateSelectedBudget(this.state.selectedBudgetIndex);
    }

    updateSelectedBudget = identifier => {
        const { budgets } = this.props;
        /* Temporary until we have the unique identifiers */
        const selectedBudgetIndex = identifier;
        // const selectedBudgetIndex = findIndex(budgets, budget => budget.identifier === identifier);
        const investments = budgets[selectedBudgetIndex].investments;

        this.setState({ selectedBudgetIndex, investments });
        this.filterInvestments({ investments });
    };

    // TODO
    filterInvestments = ({ name = 'currentYear', investments = this.state.investments }) => {
        const filteredInvestments = name === 'currentYear' ? investments.filter(i => new Date(i.dateTime).getFullYear() === 2021) : investments;

        this.setState({ selectedFilter: name, filteredInvestments });
    };

    render() {
        const { budgets, onClose } = this.props;
        const { selectedBudgetIndex, filteredInvestments, currentYear, selectedFilter } = this.state;
        return (
            <Modal className="budget-details" onClose={onClose}>
                <div className="budget-overview">
                    <Text type={Text.types.PRIMARY} size={Text.sizes.BIG}>
                        {Strings['budget.overview.header']}
                    </Text>
                    <div className="budget-cards">
                        {budgets.map(budget => (
                            <BudgetCard
                                key={budget.identifier}
                                title={budget.name}
                                period={currentYear}
                                budgetUsed={budget.budgetUsed}
                                budgetLimit={budget.budgetLimit}
                            />
                        ))}
                    </div>
                    <Text type={Text.types.SECONDARY} size={Text.sizes.SMALL} className="budget-overview-disclamer">
                        {Strings['budget.overview.disclamer']}
                    </Text>
                </div>
                <div className="budget-detials-container">
                    <div className="budget-details-header">
                        {Strings['budget.investments.header']}
                        <div className="filters">
                            <Filter
                                name="currentYear"
                                label={currentYear}
                                selected={selectedFilter === 'currentYear'}
                                onClick={this.filterInvestments}
                            />
                            <Filter
                                name="all"
                                label={Strings['budget.investments.filter.all']}
                                selected={selectedFilter === 'all'}
                                onClick={this.filterInvestments}
                            />
                        </div>
                    </div>

                    <div className="budget-details">
                        <div className="budget-list">
                            {budgets.map((budget, index) => (
                                <BudgetCard
                                    key={index}
                                    className="budget-list-item"
                                    // replace with identifier once we have all bugdets in place
                                    id={index}
                                    title={budget.name}
                                    budgetUsed={budget.budgetUsed}
                                    selected={selectedBudgetIndex === index}
                                    onClick={this.updateSelectedBudget}
                                />
                            ))}
                        </div>
                        <InvestmentsList
                            title={Strings['budget.investments.list-header']}
                            investments={filteredInvestments}
                        />
                    </div>
                </div>
            </Modal>
        );
    }
}

export default connect(
    ({ budget }) => ({
        ...budget,
        budgets: [...budget.data],
    }),
    null,
)
