import React from 'react';
import { Provider } from 'react-redux';
import { render } from 'react-dom';
import { PersistGate } from 'redux-persist/integration/react';
import { Router } from 'containers';
import { LocalizationProvider } from 'hoc';
import { setInitialLoaderText } from 'helpers';
import { createStore } from './store';

import './index.css';

setInitialLoaderText('loading.app');

const { store, persistor } = createStore();

render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <LocalizationProvider>
                <Router />
            </LocalizationProvider>
        </PersistGate>
    </Provider>,
    document.getElementById('app'),
);
