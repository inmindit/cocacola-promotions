import React, { Component } from 'react';

import { Button, Card, ListItem, Modal, Text } from 'components/common';
import { SupportCard } from 'components';
import { Strings } from 'helpers';

import './tool-details.css';

export default class ToolDetails extends Component {
    state = { services: [] };

    componentDidMount() {
        this.mapServices();
    }

    mapServices = () => {
        const services = this.props.selectedTool.services.map(service => (
            <ListItem
                key={service.identifier}
                {...service}
                imageUrl={service.icon}
                url={null}
                helpUrl={null}
                onImageClick={() => {
                    window.location.assign(Strings.formatString(service.url, { sapNumber: this.props.selectedCustomer.sapNumber || '', brands: this.props.selectedProduct.id || '' }));
                }}
            />
        ));

        this.setState({ services });
    };

    renderButtons = () => {
        return (
            <Modal.Buttons>
                <Button.Close onClick={this.props.onClose} />
            </Modal.Buttons>
        );
    };

    render() {
        const { selectedTool } = this.props;
        
        return (
            <Modal buttons={this.renderButtons()} onClose={this.props.onClose}>
                    <div className="tool-details" contentClassName="tool-details-content">
                        <div className="heading">
                            <Text.Bold size={Text.sizes.BIG}>{selectedTool.title}</Text.Bold>
                            <Text className="tool-description">{selectedTool.description}</Text>
                        </div>
                        <div className="services">
                            <Text.Bold className="services-title" size={Text.sizes.MEDIUM}>
                                {Strings['tools.services.title']}
                            </Text.Bold>
                            <div>{this.state.services}</div>
                        </div>
                        <SupportCard email="" telephone="03513070922" />
                    </div>
            </Modal>
        );
    }
}
