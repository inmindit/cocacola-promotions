import React from 'react';
import { ImageCircle, Text /* Help */ } from 'components/common';
import './services-card.css';

const ServicesCard = ({
    title,
    secondTitle,
    icon,
    textContent,
    // onHelpClick,
    onImageClick,
}) => (
    <div className="services-card-container" onClick={onImageClick}>
        <div className="services-card-first-row">
            <ImageCircle className="services-card-image" imageUrl={icon} />
            <div className="services-card-headlines">
                <Text className="services-card-title" size={Text.sizes.SMALL} transform={Text.transform.UPPERCASE}>{title}</Text>
                <Text className="services-card-second-title" size={Text.sizes.MEDIUM} transform={Text.transform.UPPERCASE}>{secondTitle}</Text>
            </div>
            {/* <Help onHelpClick={onHelpClick} /> */}
        </div>
        <div className="services-card-second-row">
            <Text>{textContent}</Text>
        </div>
    </div>
);

export default ServicesCard;

