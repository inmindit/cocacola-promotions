import React, { useState, useEffect } from 'react';
import { sortBy } from 'lodash';
import { Modal, Text } from 'components/common';
import { Strings, formatSum } from 'helpers';
import { InvestmentsList } from 'components';
import './investment-details.css';

const InvestmentDetails = ({
    subBudgets, subBudgetName, onSubBudgetChange, onClose, period,
}) => {
    const [investments, setInvestments] = useState([]);
    const [budgetUsed, setBudgetUsed] = useState(0);
    const [count, setCount] = useState(0);

    useEffect(() => {
        if (subBudgetName !== undefined) {
            let investments = [];
            let usedBudgetAmount = 0;

            if (subBudgetName === 'Alle') {
                investments = subBudgets.flatMap(subBudget => subBudget.services.flatMap(service => service.investments.map(investment => ({
                    ...investment,
                    subBudgetName: subBudget.name,
                    serviceName: service.title,
                }))));
                usedBudgetAmount = subBudgets.map(subBudget => subBudget.amount).reduce((a, b) => a + b).toFixed(0);
            } else {
                const subBudget = subBudgets.find(subBudget => subBudget.name === subBudgetName);
                investments = subBudget.services.flatMap(service => service.investments.map(investment => ({
                    ...investment,
                    subBudgetName: subBudget.name,
                    serviceName: service.title,
                })));
                usedBudgetAmount = subBudget.amount.toFixed(0);
            }

            setInvestments(investments);
            setBudgetUsed(usedBudgetAmount);
            setCount(investments.length);
        }
    }, [subBudgets, subBudgetName]);

    return (
        <Modal onClose={onClose}>
            <div className="investment-details-container">
                <Text type={Text.types.SECONDARY} size={Text.sizes.REGULAR}>
                    {Strings['page.investmentDetails.title']}
                </Text>
                <div className="investment-details-title">
                    <Text.Bold size={Text.sizes.BIG}>
                        {period === 'all' ? (Strings['page.investmentDetails.title.budgetcategories.all']) :
                            (Strings['page.investmentDetails.title.budgetcategories.currentYear']).replace('$currentYear', period)}
                    </Text.Bold>
                </div>
                <div className="investment-details-content">
                    <Text.Bold size={Text.sizes.BIG}>{Strings['page.investmentDetails.label.budgetcategorie']}:</Text.Bold>
                    <select
                        value={subBudgetName}
                        className="investment-details-select"
                        onChange={e => onSubBudgetChange(e.target.value)}
                    >
                        <option>Alle</option>
                        {
                            sortBy(subBudgets, ['name']).map(subBudget => (
                                <option key={subBudget.identifier} value={subBudget.name}>
                                    {subBudget.name}
                                </option>
                            ))
                        }


                    </select>
                    <div className="investment-details-sum-labels">
                        <Text
                            className="investments-details-amount"
                            size={Text.sizes.MEDIUM}
                            transform={Text.transform.UPPERCASE}
                        >
                            {Strings['page.budgetModal.label.sum']}
                            <Text.Bold size={Text.sizes.MEDIUM}>{`${' '} ${formatSum(budgetUsed)}`}</Text.Bold>
                        </Text>
                        <Text size={Text.sizes.MEDIUM} transform={Text.transform.UPPERCASE}>
                            {Strings['page.budgetModal.label.total']}
                            <Text.Bold size={Text.sizes.MEDIUM}>{`${' '} ${count}`} </Text.Bold>
                        </Text>
                    </div>
                </div>
                <InvestmentsList
                    investments={investments}
                    subBudgetName={subBudgetName}
                    subBudget={subBudgets}
                />
            </div>
        </Modal>
    );
};

export default InvestmentDetails;
