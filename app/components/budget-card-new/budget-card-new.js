import React, { Fragment } from 'react';
import { Card, ImageCircle, ProgressBar, Text } from 'components/common';
import { Strings, formatSum } from 'helpers';
import CountUp from 'react-countup';

import './budget-card-new.css';

const BudgetCardNew = ({
    className, title, period, budgetUsed, budgetLimit, id, selected, titleImgUrl, onClick,
}) => (
    <Card
        className={`budget-new-container ${className}`}
        contentClassName="budget-new-card"
        selected={selected}
        onClick={() => {
            onClick(id);
        }}
    >
        <div>
            <div className="budget-new-card-header">
                {titleImgUrl && <ImageCircle className="budget-new-badge" imageUrl={titleImgUrl} />}
                <Text.Medium className="budget-new-title" size={Text.sizes.SMALL}>{title}</Text.Medium>
                {period && <span className="budget-new-period">{period}</span>}
            </div>
            <div>
                <div className="budget-new-progress-container">
                    <div>
                        <Fragment>
                            <ProgressBar className="budget-new-progress" amount={budgetUsed} total={budgetLimit} />
                            <div className="flex-between">
                                <Text type={Text.types.SECONDARY} size={Text.sizes.SMALL} >
                                    {Strings['budget.card.new.available']}
                                </Text>
                                <Text type={Text.types.SECONDARY} size={Text.sizes.SMALL} >
                                    {Strings['budget.card.new.used']}
                                </Text>
                            </div>
                            <div className="flex-between">
                                <Text.Medium size={Text.sizes.SMALL}>{budgetLimit !== undefined ? `${(formatSum((budgetLimit - budgetUsed).toFixed(0)))}` : '-'}</Text.Medium>
                                {budgetLimit !== undefined ? (
                                    <Text.Medium size={Text.sizes.SMALL}>
                                        <CountUp
                                            start={0}
                                            end={budgetLimit ? ((budgetUsed / budgetLimit) * 100) : budgetUsed}
                                            duration={2}
                                        >
                                            {Math.round(budgetLimit ? (budgetUsed / budgetLimit) * 100 : budgetUsed)}
                                        </CountUp>
                                        { '\u0025'}
                                    </Text.Medium >
                                ) : (
                                    <Text.Medium size={Text.sizes.SMALL}>{`${formatSum(budgetUsed)}`}</Text.Medium>
                                )}
                            </div>
                        </Fragment>
                    </div>
                </div>
            </div>
        </div>
    </Card>
);

BudgetCardNew.defaultProps = {
    className: '',
    titleImgUrl: '',
    url: '',
};

export default BudgetCardNew;
