import React from 'react';
import { Carousel, Text } from 'components/common';
import { ProductCard, Section } from 'components';
import { Strings } from 'helpers';
import './product-section.css';

const ProductSection = ({
    selectedProduct,
    closeSelectedProduct,
    productData,
    onProductClicked,
    brandLocation,
}) => (
    <div className={selectedProduct.id ? 'product-section-selected-product' : 'product-section-not-selected-product'}>
        <div className="product-section-overview" >
            <Text
                className="product-section-name"
                type={Text.types.PRIMARY}
                size={Text.sizes.BIG}
                transform={Text.transform.UPPERCASE}
            >
                {Strings['product.section.name']}
            </Text>
            <Text
                className="product-section-title"
                size={Text.sizes.MEDIUM}
            >
                {
                    selectedProduct.id
                        ? `${Strings['product.section.active.product']} ${selectedProduct.name}`
                        : `${Strings['product.section.unactive.product']}`
                }
            </Text>
        </div>
        <div className="product-section-cards">
            {selectedProduct.image &&
            <div className="col col-left">
                <ProductCard
                    className="selected-product"
                    key={selectedProduct.name}
                    imageId={selectedProduct.image.id}
                    productName={selectedProduct.name}
                    onClick={closeSelectedProduct}
                />
            </div>
            }
            <div className={`col ${selectedProduct.image ? 'col-right' : 'col-full'}`}>
                <Carousel
                    dots={false}
                    infinite={false}
                    speed={500}
                    slidesToShow={brandLocation ? 9 : 10}
                    slidesToScroll={1}

                >
                    {productData && productData.map(product => (
                        <ProductCard
                            key={product.name}
                            imageId={product.image.id}
                            productName={product.name}
                            onClick={() => onProductClicked(product.id)}
                            id={product.id}
                        />
                    ))}
                </Carousel>
            </div>
        </div>

    </div>
);

export default ProductSection;
