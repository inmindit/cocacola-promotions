import React from 'react';
import { Icon, Text } from 'components/common';
import AuthenticatedImage from '../common/authenticated-image/authenticated-image';
import './message.css';

import cokeLogo from '../../../assets/cokeLogo.jpg';

const Message = ({
    onClose,
    message,
    imageURL,
    theme,
    isError,
}) => (
    <div className={`message-container ${theme}`}>
        {imageURL && (
            <div className="message-logo">
                <AuthenticatedImage alt="" src={imageURL || cokeLogo} />
            </div>
        )}
        <Icon className="message-close-icon" name="Close" onClick={onClose} />
        <div className={`message-text ${isError ? 'error' : ''}`}>
            <Text.Medium dangerouslySetInnerHTML={{ __html: message }} />
        </div>
    </div>
);
export default Message;

