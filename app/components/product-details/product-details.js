import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Badge } from 'components/common';

import './product-details.css';

const ProductDetails = ({ name, image = {}, onDeleteProductClick }) => (
    <div className="product-details">
        <Badge className="product-badge" imageUrl={image.id} />
        <div className="label-container">
            {name}
            <div className="delete-product">
                <Icon name="Close" onClick={onDeleteProductClick} />
            </div>
        </div>
    </div>
);

ProductDetails.propTypes = {
    name: PropTypes.string.isRequired,
    onDeleteProductClick: PropTypes.func.isRequired,
};

export default ProductDetails;
