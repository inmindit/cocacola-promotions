import React from 'react';
import { Strings } from 'helpers';
import { Text, Icon } from 'components/common';
import './customer-info-modal.css';

const CustomerInfoModal = ({
    data: {
        name1, address = {}, stc, firstPhone, secondPhone, email, contactName, classification,
    },
    hasData,
    className,
    onCloseClick,
}) => (
    hasData &&
    <div className="customer-info-modal">
        <div className={className}>
            <Icon size="big" className="close-customer-info-modal" name="Close" onClick={onCloseClick} />
            <Text size={Text.sizes.SMALL}>{Strings['customer.name']}</Text>
            <Text className="customer-name name" transform={Text.transform.UPPERCASE} size={Text.sizes.MEDIUM}>{name1}</Text>

            <div className="info-section">
                <Text size={Text.sizes.BIG} type={Text.types.SECONDARY} transform={Text.transform.UPPERCASE}>
                    {address.city}
                </Text>
                <Text className="customer-location" size={Text.sizes.MEDIUM} type={Text.types.SECONDARY}>
                    <a
                        className="customer-link"
                        href={`https://www.google.bg/maps/search/${address.street} ${address.houseNumber}, ${address.zipCode}+${address.city}`}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <Icon className="customer-icon" size="small" name="LocationOnOutlined" />
                        <span>{`${address.street} ${address.houseNumber}, ${address.zipCode} ${address.city}`}</span>
                    </a>
                </Text>
            </div>

            <div className="info-section">
                <Text type={Text.types.SECONDARY} size={Text.sizes.SMALL}>{Strings['customer.info.contact_name']}</Text>
                <Text className="customer-contact-name" transform={Text.transform.UPPERCASE} size={Text.sizes.BIG}>{contactName}</Text>

                <a className="customer-link email" href={`mailto:${email}`}>
                    <Icon className="customer-icon" size="small" name="EmailOutlined" />
                    <Text size={Text.sizes.MEDIUMBIG}>{email}</Text>
                </a>
                <a className="customer-link phone" href={`tel:${firstPhone}`}>
                    <Icon className="customer-icon" size="small" name="LocalPhoneOutlined" />
                    <Text size={Text.sizes.MEDIUMBIG}>{firstPhone}</Text>
                </a>
                <a className="customer-link phone" href={`tel:${secondPhone}`}>
                    <Icon className="customer-icon" size="small" name="LocalPhoneOutlined" />
                    <Text size={Text.sizes.MEDIUMBIG}>{secondPhone}</Text>
                </a>
            </div>

            <div className="info-section">
                <Text.Labeled
                    labelKey="customer.info.stc"
                    value={`${stc.code} - ${stc.displayName}`}
                    valueProps={{ size: Text.sizes.MEDIUMBIG }}
                />
                <Text.Labeled
                    className="detail"
                    labelKey="customer.info.customer_group"
                    value={Strings[`customer.classification.${classification}`] || classification}
                    valueProps={{ size: Text.sizes.MEDIUMBIG }}
                />
            </div>
        </div>
    </div>
);

CustomerInfoModal.defaultProps = {
    data: {},
    onCloseClick: null,
};

export default CustomerInfoModal;
