import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'components/common';

import './modal.css';

const Modal = ({
    className,
    contentClassName,
    children,
    buttons,
    onClose,
}) => (
    <div className={`modal ${className}`}>
        <div className="modal-container">
            <Icon className="close-icon" name="Close" onClick={onClose} />
            <div className={`modal-content ${contentClassName}`}>
                {children}
            </div>
            {buttons}
        </div>
    </div>
);

Modal.Buttons = ({ className, children, ...props }) => (
    <div className={`modal-buttons ${className}`} {...props}>
        {children}
    </div>
);

Modal.Buttons.defaultProps = {
    className: '',
    children: null,
};

Modal.propTypes = {
    className: PropTypes.string,
    contentClassName: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
    buttons: PropTypes.element,
    onClose: PropTypes.func.isRequired,
};

Modal.defaultProps = {
    className: '',
    contentClassName: '',
    children: null,
    buttons: null,
};

export default Modal;
