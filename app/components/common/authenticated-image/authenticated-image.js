import React, { PureComponent } from 'react';
import axios from 'axios';
import { Modal } from 'components/common';

class AuthenticatedImage extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            src: '',
            contentType: '',
        };
    }

    componentDidMount() {
        this.updateImage();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.src !== this.props.src) {
            this.updateImage();
        }
    }

    updateImage() {
        const { src } = this.props;

        this.setState({ src: '', contentType: '' });

        const request = {
            url: src,
            method: 'GET',
            responseType: 'blob',
            headers: {
                Authorization: `Token ${localStorage.getItem('COKE_AGENT.accessToken')}`,
            },
        };

        axios.request(request)
            .then(response => {
                const url = URL.createObjectURL(response.data);
                this.setState({ src: url, contentType: response.headers['content-type'] });
            });
    }

    render() {
        const { src } = this.state;

        return (
            <img {...this.props} src={src} />
        );
    }
}

export default AuthenticatedImage;
