import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Strings } from 'helpers';
import { Loader } from 'hoc';
import { Text, Icon } from 'components/common';

import './button.css';

const Button = ({
    className, textKey, textString, disabled, shouldRenderLoader, onClick, buttonType, leftIcon, rightIcon, fullWidth, link, children,
}) => {
    const Component = link ? Link : 'button';
    const linkProps = link ? { to: link } : {};

    return (
        <Component
            className={`button ${buttonType} ${className} ${fullWidth ? 'full-width' : ''}`}
            disabled={disabled}
            onClick={onClick}
            {...linkProps}
        >
            <Loader isLoading={shouldRenderLoader}>
                {leftIcon}
                <Text.Bold>{ textKey ? Strings[textKey] : textString}</Text.Bold>
                {children}
                {rightIcon}
            </Loader>
        </Component>
    );
};

Button.Text = ({ className = '', ...props }) => <Button {...props} className={`text ${className}`} />;

Button.Outlined = ({ className = '', ...props }) => <Button {...props} className={`outlined ${className}`} />;

Button.Contained = ({ className = '', ...props }) => <Button {...props} className={`contained ${className}`} />;

Button.defaultProps = {
    textKey: '',
    link: null,
    className: '',
    disabled: false,
    shouldRenderLoader: false,
    buttonType: 'flat',
    leftIcon: null,
    rightIcon: null,
    onClick: () => {},
};

Button.propTypes = {
    link: PropTypes.string,
    className: PropTypes.string,
    textKey: PropTypes.string,//.isRequired,
    disabled: PropTypes.bool,
    shouldRenderLoader: PropTypes.bool,
    onClick: PropTypes.func,
    leftIcon: PropTypes.element,
    rightIcon: PropTypes.element,
    buttonType: PropTypes.oneOf(['raised', 'flat']),
};

/* Specific buttons */

Button.Close = props => <Button.Outlined className="close" textKey="close" leftIcon={<Icon name="Close" />} {...props} />;

Button.Close.propTypes = {
    ...Button.propTypes,
    textKey: PropTypes.string,
};

Button.Choose = props => <Button.Contained className="choose" textKey="button.choose" leftIcon={<Icon size="small" name="Check" />} {...props} />;

Button.Choose.propTypes = {
    ...Button.propTypes,
    textKey: PropTypes.string,
};

export default Button;
