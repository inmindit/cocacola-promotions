import React from 'react';
import {
    Close,
    SupervisorAccountRounded,
    SecurityOutlined,
    Language,
    ExitToApp,
    ChevronLeft,
    ChevronRight,
    Menu,
    Dashboard,
    Poll,
    Notifications,
    LocalGroceryStore,
    ExpandMore,
    Check,
    Add,
    LocationOnOutlined,
    EmailOutlined,
    LocalPhoneOutlined,
    HelpOutlineOutlined,
    KeyboardArrowDown,
    ContactSupportOutlined,
    AccountCircleOutlined,
    Input,
    CalendarToday,
    Search,
    Info,
    HelpOutline,
} from '@material-ui/icons';
import './icon.css';

const AvailableIcons = {
    Close,
    SupervisorAccountRounded,
    SecurityOutlined,
    Language,
    ExitToApp,
    ChevronLeft,
    ChevronRight,
    Menu,
    Dashboard,
    Poll,
    Notifications,
    LocalGroceryStore,
    ExpandMore,
    Check,
    Add,
    LocationOnOutlined,
    EmailOutlined,
    LocalPhoneOutlined,
    HelpOutlineOutlined,
    KeyboardArrowDown,
    ContactSupportOutlined,
    AccountCircleOutlined,
    Input,
    CalendarToday,
    Search,
    Info,
    HelpOutline,
};

const Icon = ({
    name, size, className, isWrapped, wrapperClassName, ...props
}) => {
    const Component = AvailableIcons[name];

    const icon = <Component className={`icon ${size} ${className}`} {...props} />;

    if (isWrapped) {
        return (
            <span className={wrapperClassName}>
                {icon}
            </span>
        );
    }

    return icon;
};

Icon.defaultProps = {
    name: '',
    className: '',
    size: '',
    isWrapped: false,
    wrapperClassName: '',
};

export default Icon;
