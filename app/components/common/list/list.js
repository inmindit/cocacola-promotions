import React from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'helpers';
import { Text } from 'components/common';

import './list.css';

const List = ({
    className, itemClassName, listItemsClassName, title, items,
}) => (
    <ul className={`list ${className}`}>
        <div className="title">
            <Text>{Strings[title] || title}</Text>
        </div>
        <div className={`list-items-container ${listItemsClassName}`}>
            {items.map(item => (
                <li className={`list-item ${itemClassName}`} key={item}>
                    {item}
                </li>
            ))}
        </div>
    </ul>
);

List.propTypes = {
    items: PropTypes.arrayOf(PropTypes.string).isRequired,
    className: PropTypes.string,
    itemClassName: PropTypes.string,
    listItemsClassName: PropTypes.string,
    title: PropTypes.string,
};

List.defaultProps = {
    className: '',
    itemClassName: '',
    listItemsClassName: '',
    title: '',
};

export default List;
