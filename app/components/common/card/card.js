import React from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'helpers';
import { Text, Loader } from 'components/common';

import './card.css';

const Card = ({
    isLoading, className, contentClassName, title, selected, children, contentMaxHeight, onClick,
}) => {
    if (isLoading) {
        return <Loader />;
    }

    return (
        <div className={`card ${className} ${selected ? 'selected' : ''}`} onClick={onClick}>
            {title && (
                <div className="title-container">
                    <Text>{Strings[title]}</Text>
                </div>
            )}
            <div style={{ maxHeight: contentMaxHeight, marginTop: title ? 16 : 0 }} className={`content-container ${contentClassName}`}>
                {children}
            </div>
        </div>
    );
};

Card.propTypes = {
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
    className: PropTypes.string,
    contentClassName: PropTypes.string,
    title: PropTypes.string,
    contentMaxHeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    selected: PropTypes.bool,
    onClick: PropTypes.func,
    isLoading: PropTypes.bool,
};

Card.defaultProps = {
    className: '',
    contentClassName: '',
    title: '',
    contentMaxHeight: null,
    selected: false,
    onClick: null,
    isLoading: false,
};

export default Card;
