import React from 'react';
import PropTypes from 'prop-types';

import './progress-bar.css';

const progressBarColor = (amount, total) => {
    const progressPercentage = (amount / total) * 100;
    if (progressPercentage <= 50) {
        return 'progress-green';
    }
    if (progressPercentage > 50 && progressPercentage <= 80) {
        return 'progress-orange';
    }
    if (progressPercentage > 80) {
        return 'progress-red';
    }
};

const ProgressBar = ({ className, total, amount }) => (
    <div className={`progress-bar-container ${className}`}>
        <div
            className={progressBarColor(amount, total)}
            style={{ width: `${(amount > total) ? 100 : (amount / total) * 100}%` }}
        />
    </div>
);

ProgressBar.propTypes = {
    amount: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired,
    className: PropTypes.string,
};

ProgressBar.defaultProps = {
    className: '',
};

export default ProgressBar;
