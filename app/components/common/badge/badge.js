import React from 'react';
import PropTypes from 'prop-types';

import { ImageCircle } from 'components/common';

import './badge.css';

const Badge = ({ className, imageUrl }) => (
    <div className={`badge-container ${className}`}>
        <ImageCircle imageUrl={imageUrl} />
    </div>
);

Badge.propTypes = {
    imageUrl: PropTypes.string,
    className: PropTypes.string,
};

Badge.defaultProps = {
    className: '',
};

export default Badge;
