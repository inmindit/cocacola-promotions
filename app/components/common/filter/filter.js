import React from 'react';

import { Icon } from 'components/common';

import './filter.css';

const Filter = ({
    name, className, label, selected, iconName, onClick,
}) => (
    <div
        className={`filter ${selected ? 'selected' : ''} ${className}`}
        onClick={() => {
            onClick({ name });
        }}
    >
        <Icon name={iconName} />
        {label}
    </div>
);

Filter.defaultProps = {
    className: '',
    iconName: 'CalendarToday',
};

export default Filter;
