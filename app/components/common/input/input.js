import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { Strings } from 'helpers';

import './input.css';

const Input = ({
    className, placeholder, leftIcon, required, rightIcon, error, children, ...rest
}) => (
    <div className={`input ${className}`}>
        <TextField classes={{ root: 'input-root' }} {...rest} error={!!error} InputLabelProps={{ disableAnimation: true, shrink: true, required }}>
            {children}
        </TextField>
        {error && <div className="input-error">{Strings[error]}</div>}
    </div>
);

Input.defaultProps = {
    className: '',
    placeholder: '',
    leftIcon: null,
    rightIcon: null,
    variant: 'outlined',
    fullWidth: true,
};

Input.propTypes = {
    className: PropTypes.string,
    placeholder: PropTypes.string,
    leftIcon: PropTypes.element,
    rightIcon: PropTypes.element,
    variant: PropTypes.string,
    fullWidth: PropTypes.bool,
};

export default Input;
