import React from 'react';
import Slider from 'react-slick';

// import './slider-slick.css';


const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
};

const SliderSlick = ({
    children,
}) => (
    <Slider {...settings}>{children}</Slider>
);

export default SliderSlick;
