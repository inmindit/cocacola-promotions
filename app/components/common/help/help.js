import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'components/common';

import './help.css';

const Help = ({
    className, iconClassName, url, onHelpClick,
}) => {
    if (url) {
        return (
            <a href={url} className={`help-container ${className}`}>
                <Icon name="HelpOutlineOutlined" className={iconClassName} />
            </a>
        );
    }

    return (
        <div className={`help-container ${className}`} >
            <Icon name="HelpOutlineOutlined" className={iconClassName} onClick={onHelpClick} />
        </div>
    );
};

Help.propTypes = {
    className: PropTypes.string,
    iconClassName: PropTypes.string,
    url: PropTypes.string,
};

Help.defaultProps = {
    className: '',
    iconClassName: '',
    url: '',
};

export default Help;
