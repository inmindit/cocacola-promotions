import React from 'react';
import { Strings } from 'helpers';

import './text.css';

const Text = ({
    className = '', size, transform, type, children, ...rest
}) => (
    <span {...rest} className={`text ${className} ${size} ${type} ${transform}`}>
        {children}
    </span>
);

const LabeledText = ({
    className, labelKey, label, value, inline, labelProps = {}, valueProps = {},
}) => (
    <div className={`labeled-text ${className} ${inline ? 'inline' : ''}`}>
        <Text type={Text.types.SECONDARY} size={Text.sizes.SMALL} {...labelProps}>
            {label || Strings[labelKey]}
        </Text>
        <Text className="text-value" {...valueProps}>
            {value}
        </Text>
    </div>
);

LabeledText.defaultProps = {
    className: '',
};

const BoldText = ({ className = '', ...props }) => <Text {...props} className={`bold ${className}`} />;

const MediumText = ({ className = '', ...props }) => <Text {...props} className={`weight-medium ${className}`} />;


BoldText.defaultProps = {
    className: '',
};

MediumText.defaultProps = {
    className: '',
};

Text.Labeled = props => <LabeledText {...props} />;

Text.Bold = props => <BoldText {...props} />;

Text.Medium = props => <MediumText {...props} />;

Text.Bold.displayName = 'Text.Bold';

Text.Medium.displayName = 'Text.Medium';

Text.Labeled.displayName = 'Text.Labeled';

Text.sizes = {
    MIN: 'min',
    SMALL: 'small',
    REGULAR: 'regular',
    MEDIUM: 'medium',
    MEDIUMBIG: 'medium-big',
    BIG: 'big',
    MAX: 'max',
};

Text.types = {
    PRIMARY: 'primary',
    SECONDARY: 'secondary',
};

Text.transform = {
    NONE: 'none',
    UPPERCASE: 'uppercase',
    CAPITALIZE: 'capitalize',
};

Text.defaultProps = {
    className: '',
    size: Text.sizes.REGULAR,
    transform: Text.transform.NONE,
    type: Text.types.PRIMARY,
};

export default Text;
