import React from 'react';
import Slider from 'react-slick';
import './carousel.css';


const Carousel = ({
    children,
    dots,
    infinite,
    speed,
    slidesToShow,
    slidesToScroll,
}) => (
    <Slider
        dots={dots}
        infinite={infinite}
        speed={speed}
        slidesToScroll={slidesToScroll}
        slidesToShow={slidesToShow}
        swipeToSlide
        nextArrow={<div className="arrow-next" />}
        prevArrow={<div className="arrow-prev" />}
    >
        {children}
    </Slider>
);

Carousel.defaultProps = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
};

export default Carousel;
