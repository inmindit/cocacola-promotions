import React from 'react';
import PropTypes from 'prop-types';
import AuthenticatedImage from '../authenticated-image/authenticated-image';
import coke from './coke.jpg';

import './image-circle.css';

const ImageCircle = ({ className, imageUrl, onClick }) => {
    const fullUrl = imageUrl && imageUrl.match('http');

    return (
        <div className={`image-cirle ${className}`} onClick={onClick}>
            {fullUrl ? <img alt="" src={imageUrl || coke} /> : <AuthenticatedImage alt="" src={imageUrl ? `/api/documentFile/${imageUrl}` : coke} />}
        </div>
    );
};

ImageCircle.propTypes = {
    imageUrl: PropTypes.string.isRequired,
    className: PropTypes.string,
};

ImageCircle.defaultProps = {
    className: '',
};

export default ImageCircle;
