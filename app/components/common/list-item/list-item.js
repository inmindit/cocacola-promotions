import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Icon, ImageCircle, Text } from 'components/common';
import { Strings } from 'helpers';

import './list-item.css';

const ListItem = ({
    className, title, description, imageUrl, url, urlExternal, onImageClick,
}) => {
    const itemContent = (
        <Fragment >
            <ImageCircle className="list-item-icon" imageUrl={imageUrl} />
            <div className="list-item-info" >
                <Text.Bold transform={Text.transform.UPPERCASE} size={Text.sizes.MEDIUM}>
                    {title}
                </Text.Bold>
                <Text type={Text.types.SECONDARY} size={Text.sizes.REGULAR}>{description}</Text>
            </div>
            <div className="help-container">
                <Icon className="list-info-icon" name="Info" />
                <Text size={Text.sizes.MEDIUM} transform={Text.transform.UPPERCASE}>{Strings['list.item.handbook']}</Text>
                <Icon className="list-arrow-right" name="ChevronRight" />
            </div>
        </Fragment>
    );

    const LinkComponent = urlExternal ? 'a' : Link;
    const linkProps = urlExternal ? { href: url } : { to: url };

    return (
        <div className={`list-item ${className}`} onClick={onImageClick} >
            {url ? (
                <LinkComponent className="list-item-url" {...linkProps}>
                    {itemContent}
                </LinkComponent>
            ) : (
                itemContent
            )}
        </div>
    );
};

ListItem.propTypes = {
    className: PropTypes.string,
    url: PropTypes.string,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    imageUrl: PropTypes.string,
    onClick: PropTypes.func,
};

ListItem.defaultProps = {
    className: '',
    url: '',
    onClick: null,
};

export default ListItem;
