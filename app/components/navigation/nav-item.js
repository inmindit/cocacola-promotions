import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { Strings } from 'helpers';
import { Icon } from 'components/common';

import './navigation.css';

const NavItem = ({
    className, itemKey, labelKey, icon, disabled, selected, expanded, onClick, labelString, iconString, children, url,
}) => {
    const itemContent = (
        <Fragment>
            {icon && <Icon name={icon} className="nav-icon" /> }
            {iconString && <div>{iconString}</div>}
            {children && children}
            {expanded && <span className="nav-item-label">{labelKey ? Strings[labelKey] : labelString}</span>}
        </Fragment>
    );

    const wrapperClassName = `nav-item-wrapper ${disabled ? 'disabled' : ''}`;

    return (
        <div className={`nav-item ${className} ${selected ? 'selected' : ''}`} onClick={() => { onClick(itemKey); }}>
            {url ? (
                <Link to={url} className={wrapperClassName}>
                    {itemContent}
                </Link>
            ) : (
                <div className={wrapperClassName}>{itemContent}</div>
            )}

        </div>
    );
};

NavItem.propTypes = {
    labelKey: PropTypes.string,
    icon: PropTypes.string,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    url: PropTypes.string,
    selected: PropTypes.bool,
    onClick: PropTypes.func,
};

NavItem.defaultProps = {
    labelKey: '',
    icon: '',
    className: '',
    url: '',
    disabled: false,
    selected: false,
    onClick: () => {},
};

export default NavItem;
