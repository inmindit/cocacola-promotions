import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Strings } from '../../helpers';
import { Icon } from 'components/common';
import { logout } from '../../actions/user';
import NavItem from './nav-item';
import PdfFile from '../../../assets/210608_W1_Leporello_Digital_2290x1080px.pdf';

import './navigation.css';

const Navigation = ({
    expanded, selected, items, headerText, headerIconName, toggleExpand, onItemClick, userData, onLogoutClick,
}) => (
    <div className={`navigation-wrapper ${expanded ? 'expanded' : ''}`}>
        <div className="navigation-container">
            <div className="items-container">
                {headerText && (
                    <NavItem className="navigation-header" expanded={expanded} icon={headerIconName} labelKey={headerText} onClick={toggleExpand} />
                )}

                {items.map(item => (
                    <NavItem {...item} expanded={expanded} selected={selected.key === item.key} onClick={() => onItemClick(item.key)} />
                ))}
                <a href={PdfFile} style={{ textDecoration: 'none' }} target="_blank" rel="noopener noreferrer">
                    <NavItem icon="HelpOutline" labelString={Strings.help} expanded={expanded} />
                </a>
            </div>

            <div className="footer-container">
                <NavItem expanded={expanded} labelString={userData.fullName} >
                    <div className="menu-icon">{userData.shortUsername}</div>
                </NavItem>

                <NavItem className="is-link" expanded={expanded} icon="ExitToApp" labelKey="logout" onClick={onLogoutClick} />
            </div>

            <div className="navigation-footer">
                {/* <Link to={footerUrl} className="navigation-link">
                    {footerUrlText}
                </Link> */}
                <div className="toggle-button" onClick={toggleExpand}>
                    <Icon className="nav-icon" name={expanded ? 'ChevronLeft' : 'ChevronRight'} />
                </div>
            </div>
        </div>

        {expanded && <div className="overlay" />}
    </div>
);

Navigation.propTypes = {
    expanded: PropTypes.bool,
    items: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string.isRequired,
        labelKey: PropTypes.string.isRequired,
        icon: PropTypes.string.isRequired,
        disabled: PropTypes.bool,
        url: PropTypes.string,
    })).isRequired,
    toggleExpand: PropTypes.func.isRequired,
    selected: PropTypes.shape({
        key: PropTypes.string,
        labelKey: PropTypes.string,
        icon: PropTypes.string,
        disabled: PropTypes.bool,
        url: PropTypes.string,
    }).isRequired,
    footerUrl: PropTypes.string,
    footerUrlText: PropTypes.string,
};

Navigation.defaultProps = {
    expanded: false,
    footerUrl: '',
    footerUrlText: '',
};

export default connect(
    null,
    dispatch => ({
        actions: bindActionCreators(logout, dispatch),
    }),
)(Navigation);
