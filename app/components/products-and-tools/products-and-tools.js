import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ProductSection, ToolDetails } from 'components/index';
import { Text } from 'components/common';
import { Strings } from 'helpers/index';
import { Tools } from 'containers/index';
import { updateSelectedProduct } from 'actions/products';
import { updateSelectedTool } from 'actions/tools';
import { setActiveView } from 'actions/application';
import { parse, stringify } from 'qs';

const ProductsAndTools = () => {
    const dispatch = useDispatch();

    const selectedProduct = useSelector(state => state.products.selectedProduct);
    const productData = useSelector(state => state.products.products);
    const selectedProductData = productData.filter(
        product => product.id !== selectedProduct.id,
    );

    const selectedTool = useSelector(state => state.tools.selectedTool);
    const selectedCustomer = useSelector(state => state.customers.selectedCustomer);

    const activeFilter = useSelector(state => state.tools.activeFilter);

    const [showToolDetails, setShowToolDetails] = useState(false);

    useEffect(() => {
        let url = window.location.pathname;

        const queryParams = parse(window.location.search, {ignoreQueryPrefix: true});

        if (selectedProduct && selectedProduct.id) {
            queryParams.brands = selectedProduct.id;
        } else {
            delete queryParams.brands;
        }

        if (Object.keys(queryParams).length > 0) {
            url += `?${stringify(queryParams)}`;
        }

        window.history.replaceState({}, '', url);
    }, [selectedProduct]);

    const handleProductClick = productId => {
        dispatch(
            updateSelectedProduct(productId),
        );
    };

    const handleProductClose = () => {
        dispatch(
            updateSelectedProduct(null),
        );
    };

    const toggleToolDetails = () => {
        const activeView = showToolDetails ? activeFilter : 'toolDetails';

        dispatch(
            setActiveView(activeView),
        );

        setShowToolDetails(prevShowToolDetails => !prevShowToolDetails);
    };

    const handleToolClick = id => {
        dispatch(
            updateSelectedTool(id),
        );
        toggleToolDetails();
    };

    const showTools = !!selectedProduct.id;

    return (
        <div className="products-and-tools">
            <ProductSection
                brandLocation={selectedProduct.id}
                productData={selectedProductData}
                selectedProduct={selectedProduct}
                onProductClicked={handleProductClick}
                closeSelectedProduct={handleProductClose}
            />
            {
                showTools && (
                    <div>
                        <div className='tools-title-content'>
                            <div className="tools-title">
                                <Text size={Text.sizes.MEDIUM}>
                                    {Strings['tools.title']}
                                </Text>
                            </div>
                        </div>
                        <div className="second-row">
                            <Tools onToolClick={handleToolClick} />
                        </div>
                    </div>
                )
            }
            {
                showToolDetails && (
                    <ToolDetails
                        selectedTool={selectedTool}
                        selectedCustomer={selectedCustomer}
                        selectedProduct={selectedProduct}
                        onClose={toggleToolDetails}
                    />
                )
            }
        </div>
    );
};

export default ProductsAndTools;
