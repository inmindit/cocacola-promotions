import React, { Fragment } from 'react';
import { Strings } from 'helpers';
import { Text, Icon, Loader } from 'components/common';
import NoDataImage from './_thumb_11286.png';

import './customer-info.css';

const CustomerInfo = ({
    data: {
        sapNumber, name1, name2, address = {}, stc, firstPhone, secondPhone, fax, email, contactName, classification,
        zterm, ztermSp, knnrSp, directCustomer,
    },
    hasData,
    className,
    hasOverlay,
    hasCloseBtn,
    onCloseClick,
    isLoading,
}) => {
    let content = <Loader />;

    const labelValue = knnrSp ? `${knnrSp}
        ${ztermSp}` : ztermSp;

    if (!isLoading) {
        content = hasData ? (
            <Fragment>

                <div className={className}>
                    {hasCloseBtn && <Icon size="big" className="close-customer-info" name="Close" onClick={onCloseClick} />}
                    <Text.Labeled
                        labelKey="customer.info.id"
                        value={sapNumber}
                        valueProps={{ transform: Text.transform.UPPERCASE, size: Text.sizes.MEDIUM }}
                    />
                    <Text className="customer-name name1" transform={Text.transform.UPPERCASE} size={Text.sizes.MAX}>{name1}</Text>
                    <Text className="customer-name name2" transform={Text.transform.UPPERCASE} type={Text.types.SECONDARY} size={Text.sizes.BIG}>{name2}</Text>

                    <div className="info-section">
                        <Text size={Text.sizes.MEDIUM} type={Text.types.SECONDARY} transform={Text.transform.UPPERCASE}>
                            {address.city}
                        </Text>
                        <Text size={Text.sizes.MEDIUM} type={Text.types.SECONDARY}>
                            <a
                                className="customer-link"
                                href={`https://www.google.bg/maps/search/${address.street} ${address.houseNumber}, ${address.zipCode}+${address.city}`}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <Icon className="customer-icon" size="small" name="LocationOnOutlined" />
                                <span>{`${address.street} ${address.houseNumber}, ${address.zipCode} ${address.city}`}</span>
                            </a>
                        </Text>
                    </div>

                    <div className="info-section">
                        <Text.Labeled
                            labelKey="customer.info.contact_name"
                            value={contactName}
                            valueProps={{ transform: Text.transform.UPPERCASE, size: Text.sizes.BIG }}
                        />
                        <a className="customer-link email" href={`mailto:${email}`}>
                            <Icon className="customer-icon" size="small" name="EmailOutlined" />
                            <Text size={Text.sizes.MEDIUMBIG}>{email}</Text>
                        </a>
                        <a className="customer-link phone" href={`tel:${firstPhone}`}>
                            <Icon className="customer-icon" size="small" name="LocalPhoneOutlined" />
                            <Text size={Text.sizes.MEDIUMBIG}>{firstPhone}</Text>
                        </a>
                        <a className="customer-link phone" href={`tel:${secondPhone}`}>
                            <Icon className="customer-icon" size="small" name="LocalPhoneOutlined" />
                            <Text size={Text.sizes.MEDIUMBIG}>{secondPhone}</Text>
                        </a>
                    </div>

                    <div className="info-section">
                        <Text.Labeled labelKey="customer.info.stc" value={`${stc.code} - ${stc.displayName}`} valueProps={{ size: Text.sizes.MEDIUMBIG }} />
                        <Text.Labeled className="detail" labelKey="customer.info.customer_group" value={Strings[`customer.classification.${classification}`] || classification} valueProps={{ size: Text.sizes.MEDIUMBIG }} />
                    </div>

                    <div className="info-section">
                        {zterm && <Text.Labeled labelKey="customer.info.zterm" value={zterm} valueProps={{ size: Text.sizes.MEDIUMBIG }} />}
                        <Text.Labeled className="detail" labelKey="customer.info.ztermSp" value={labelValue} valueProps={{ size: Text.sizes.MEDIUMBIG }} />
                    </div>
                    <div className="info-section">
                        {[0, 1, 2].includes(directCustomer) && <Text.Labeled labelKey="customer.type" value={Strings[`customer.type.${directCustomer}`]} valueProps={{ size: Text.sizes.MEDIUMBIG }} />}
                    </div>
                </div>
                { hasOverlay && <div className="overlay" /> }
            </Fragment>
        ) : (
            <div className="customer-info-no-data">
                <img className="no-data-image" src={NoDataImage} alt="" />
            </div>
        );
    }

    return (
        <div className="customer-info">
            {content}
        </div>
    );
};

CustomerInfo.defaultProps = {
    data: {},
    hasOverlay: false,
    hasCloseBtn: false,
    onCloseClick: null,
};

export default CustomerInfo;
