import React, { PureComponent, createRef } from 'react';
import { Strings } from 'helpers';
import { Input, Icon, Text, Loader } from 'components/common';

import './customers-list.css';

const CustomerListItem = ({
    sapNumber,
    name1,
    address,
    isSelected,
    onClick,
}) => (
    <div className={`customer-list-item ${isSelected ? 'active' : ''}`} onClick={onClick}>
        <div className="number">{sapNumber}</div>
        <div className="name primary-text-color">{name1}</div>
        <div className="address">{`${address.street} ${address.houseNumber}, ${address.zipCode} ${address.city}`}</div>
    </div>
);

class CustomersList extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            search: props.search,
        };

        this.inputChange = this.inputChange.bind(this);
        this.onSearchChange = this.onSearchChange.bind(this);
        this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
    }

    inputChange() {
        const search = this.state.search;

        this.setState({ search });
        this.props.onSearchChange(search);
    }

    onSearchChange(e) {
        this.setState({ search: e.target.value });
    }

    handleSearchSubmit(e) {
        e.preventDefault();
        this.inputChange();
    }

    render() {
        const {
            isLoading,
            customers,
            selectedCustomerId,
            onCustomerClick,
        } = this.props;

        let content = <Loader />;

        if (!isLoading) {
            content = (
                <div className="customers-results">
                    { customers.map(customer => (
                        <CustomerListItem
                            {...customer}
                            key={customer.sapNumber}
                            onClick={() => onCustomerClick(customer.sapNumber)}
                            isSelected={customer.sapNumber === selectedCustomerId}
                        />
                    )) }
                </div>
            );
        }

        return (
            <div className="customers-list">
                <div className="customer-search">
                    <Icon className="customer-search-icon" name="Search" onClick={this.inputChange}/>
                    <form action="" onSubmit={this.handleSearchSubmit}>
                        <input
                            className="customer-search-input"
                            type="text"
                            placeholder={Strings['customer.search.placeholder']}
                            value={this.state.search}
                            onChange={this.onSearchChange}
                        />
                    </form>
                </div>
                {content}
            </div>
        );
    }
}

export default CustomersList;
