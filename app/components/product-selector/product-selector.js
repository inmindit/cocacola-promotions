import React from 'react';
import PropTypes from 'prop-types';

import { Badge, Card } from 'components/common';

import './product-selector.css';

const ProductSelector = ({
    className, id, selected, name, identifier, description, productUrl, onClick, image,
}) => {
    return (
        <Card
            className={`product-selector-container ${className} ${selected ? 'selected' : ''}`}
            selected={selected}
            onClick={() => {
                onClick(id);
            }}
        >
            <div className="info-container">
                <div className="product-category">{identifier}</div>
                <div className="product-name">{name}</div>
                <div className="product-description">{description}</div>
            </div>
            <Badge imageUrl={image.id} />
        </Card>
    );
};

ProductSelector.propTypes = {
    className: PropTypes.string,
    selected: PropTypes.bool,
};

ProductSelector.defaultProps = {
    className: '',
    selected: false,
};

export default ProductSelector;
