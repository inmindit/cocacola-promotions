import React, { Component } from 'react';
import { Text, Icon } from 'components/common';
import { Strings } from 'helpers';

import './section.css';


class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false,
        };

        this.carouselPrefs = {
            dots: false,
            infinite: false,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 0,
        };

        this.onExpandPressed = this.onExpandPressed.bind(this);
    }

    onExpandPressed() {
        this.setState({
            expanded: !this.state.expanded,
        });
    }

    render() {
        const { expanded } = this.state;
        const { children, headerTextKey } = this.props;
        const multipleChildren = Array.isArray(children);


        return (
            <div className="section-container">
                <div className="section-title">
                    <Text type={Text.types.PRIMARY} size={Text.sizes.BIG} transform={Text.transform.UPPERCASE} >
                        {Strings[headerTextKey]}
                    </Text>
                </div>
                <div className="section-content">
                    {multipleChildren ? children[0] : children}
                </div>
                {(expanded && multipleChildren) &&
                    children.slice(1)
                }
                {multipleChildren &&
                    <div className="section-expand-button">
                        <Icon
                            className={`section-expand-button-${expanded ? 'up' : 'down'}`}
                            onClick={this.onExpandPressed}
                            name={expanded ? 'ChevronRight' : 'ChevronRight'}
                        />
                    </div>
                }
            </div>
        );
    }
}

export default Section;
