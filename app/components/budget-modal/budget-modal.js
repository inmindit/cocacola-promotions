import React from 'react';
import { Modal, Text, ProgressBar } from 'components/common';
import { Strings, formatSum } from 'helpers';
import CountUp from 'react-countup';
import { InvestmentsList } from 'components';

import './budget-modal.css';

const BudgetModal = ({
    onClose,
    selectedBudget,
    investments,
}) => (
    <Modal onClose={onClose}>
        {selectedBudget && selectedBudget.map(selectedBudget => (
            <div className="details-container">
                <Text type={Text.types.SECONDARY} size={Text.sizes.REGULAR} transform={Text.transform.UPPERCASE}>
                    {Strings['page.budgetModal.title']}
                </Text>
                <div className="details-budget-name">
                    <Text.Bold size={Text.sizes.BIG}>
                        {selectedBudget.name}
                    </Text.Bold>
                </div>
                {!!selectedBudget.budgetLimit && (
                    <ProgressBar className="details-budget-progress" amount={selectedBudget.budgetUsed} total={selectedBudget.budgetLimit} />
                )}

                {!!selectedBudget.budgetLimit && (
                    <div className="flex-between details-amounts">
                        <Text className="details-amount">
                            <Text.Bold>{`${(formatSum((selectedBudget.budgetLimit - selectedBudget.budgetUsed).toFixed(0)))} `}</Text.Bold>
                            <Text>{`von ${formatSum(selectedBudget.budgetLimit)} ${Strings['budget.card.new.available']}`}</Text>
                        </Text>
                        <Text.Bold className="details-percentage">
                            <CountUp
                                start={0}
                                end={selectedBudget.budgetLimit ? ((selectedBudget.budgetUsed / selectedBudget.budgetLimit) * 100) : selectedBudget.budgetUsed}
                                duration={2}
                            >
                                {Math.round(selectedBudget.budgetLimit ? (selectedBudget.budgetUsed / selectedBudget.budgetLimit) * 100 : selectedBudget.budgetUsed)}
                            </CountUp>
                            { '\u0025'}
                        </Text.Bold >
                    </div>
                )}
                <div className="budget-modal-sum-labels">
                    <Text className="budget-modal-details-amount" size={Text.sizes.MEDIUM} transform={Text.transform.UPPERCASE}>{Strings['page.budgetModal.label.sum']}
                        <Text.Bold size={Text.sizes.MEDIUM}>{`${' '} ${formatSum(selectedBudget.budgetUsed.toFixed(0))}`}</Text.Bold>
                    </Text>
                    <Text size={Text.sizes.MEDIUM} transform={Text.transform.UPPERCASE} >{Strings['page.budgetModal.label.total']}
                        <Text.Bold size={Text.sizes.MEDIUM}>{`${' '} ${investments.length}`}</Text.Bold>
                    </Text>
                </div>
                <InvestmentsList investments={investments} />
            </div>
        ))}
    </Modal>
);

export default BudgetModal;
