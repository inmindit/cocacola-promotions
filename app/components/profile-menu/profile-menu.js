import React, { useState } from 'react';
import { Strings } from 'helpers';
import { Icon } from 'components/common';

import './profile-menu.css';

const DropDown = ({
    fullName,
    onLogoutClick,
}) => (
    <div className="user-dropdown">
        <div className="user-dropdown-content">
            <div><Icon name="AccountCircleOutlined" />{fullName}</div>
            <div className="devider" />
            <div onClick={onLogoutClick}><Icon name="ExitToApp" />{Strings.logout}</div>
        </div>
    </div>
);

const ProfileMenu = ({ className, username, shortUsername, onLogoutClick }) => {
    const [showDD, toggleDD] = useState(false);
    const ddClassName = showDD ? 'dd-visible' : '';

    return (
        <div className={`profile-menu-container ${className}`}>
            <div className={`user-profile ${ddClassName}`} onClick={() => toggleDD(!showDD)}>
                <div className="menu-icon">{shortUsername}</div>
                <Icon className="menu-arrow" name="ExpandMore" />
                { showDD && <DropDown fullName={username} onLogoutClick={onLogoutClick} /> }
            </div>
        </div>
    );
};

export default ProfileMenu;
