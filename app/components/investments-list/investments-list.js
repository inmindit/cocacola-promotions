import React from 'react';
import moment from 'moment';
import { orderBy } from 'lodash';
import { Strings, formatSum } from 'helpers';
import { Text, Icon } from 'components/common';

import './investments-list.css';

const InvestmentListItem = ({
    subBudgetName, title, serviceName, dateTime, amount, url = '#',
}) => (
    <div className="investment-list-item" contentClassName="investment-content">
        <a href={url} className="investment-detail link">
            <Icon name="Input" className="link-icon" />
            <div className="investment-list-item-content">
                {subBudgetName ? (
                    <div className="investment-detail-items">
                        <Text className="investment-detail-sub-budget-name">{subBudgetName}</Text>
                        <Text className="investment-detail-name">{serviceName}</Text>
                        <Text className="investment-detail-title">{title}</Text>
                        <Text className="investment-detail-date-time">{moment(new Date(dateTime)).format('DD.MM.YY')}</Text>
                        <Text className="investment-detail-amount">{`${formatSum(amount.toFixed(0))}`}</Text>
                    </div>
                ) : (
                    <div className="investment-items">
                        <Text className="investment-name">{serviceName}</Text>
                        <Text className="investment-title">{title}</Text>
                        <Text className="investment-date-time">{moment(new Date(dateTime)).format('DD.MM.YY')}</Text>
                        <Text className="investment-amount">{`${formatSum(amount.toFixed(0))}`}</Text>
                    </div>
                )}
            </div>
        </a>
    </div>

);
const InvestmentsList = ({ investments, subBudgetName }) => (
    <div className="investments-list-container">
        {subBudgetName ?
            (
                <div className="investment-list-labels">
                    <Text size={Text.sizes.MEDIUM}>{Strings['page.investmentDetails.label.budgetcategorie']}</Text>
                    <Text size={Text.sizes.MEDIUM}>{Strings['page.budgetModal.label.name']}</Text>
                    <Text size={Text.sizes.MEDIUM}>{Strings['page.budgetModal.label.id']}</Text>
                    <Text size={Text.sizes.MEDIUM}>{Strings['page.budgetModal.label.date']}</Text>
                    <Text size={Text.sizes.MEDIUM}>{Strings['page.budgetModal.label.amount']}</Text>
                </div>
            ) : (
                <div className="investment-list-labels">
                    <Text size={Text.sizes.BIG}>{Strings['page.budgetModal.label.name']}</Text>
                    <Text size={Text.sizes.BIG}>{Strings['page.budgetModal.label.id']}</Text>
                    <Text size={Text.sizes.BIG}>{Strings['page.budgetModal.label.date']}</Text>
                    <Text size={Text.sizes.BIG}>{Strings['page.budgetModal.label.amount']}</Text>
                </div>
            )
        }
        <div className="investments-list">
            {
                orderBy(
                    investments.map(investment => ({
                        ...investment,
                        sortableDate: moment(investment.dateTime),
                    })),
                    ['sortableDate'],
                    ['desc'],
                ).map(investment => <InvestmentListItem key={investment.title} {...investment} />)
            }
        </div>
    </div>
);

export default InvestmentsList;
