import React, { Component } from 'react';
import Chart from 'chart.js';
import moment from 'moment';
import { orderBy, sortBy, maxBy } from 'lodash';
import { Card, Text, Icon } from 'components/common';
import { Strings, formatSum } from 'helpers';
import './investments.css';

export default class Investments extends Component {
    investmentsCategoryChart = React.createRef();
    investmentsMonthYearChart = React.createRef();

    getSubBudgetsData = () => {
        return this.props.subBudgets.map(
            subBudget => ({
                label: subBudget.name,
                amount: subBudget.amount,
            })
        );
    }

    getServices = () => {
        return this.props.subBudgets.flatMap(subBudget => subBudget.services);
    }

    getInvestments = () => {
        return this.getServices().flatMap(service => service.investments);
    }

    getInvestmentData = () => {
        if (this.props.period === 'all') {
            return this.getInvestmentsDataByYear();
        }

        return this.getInvestmentsDataByMonth();
    }

    getInvestmentsDataByYear = () => {
        return sortBy(
            this.getInvestments().reduce(
                (groupedInvestments, investment) => {
                    const investmentYear = moment(investment.dateTime).format('YYYY');
                    let group = groupedInvestments.find(group => group.label === investmentYear);

                    if (group === undefined) {
                        group = {
                            label: investmentYear,
                            amount: 0,
                        };
                        groupedInvestments.push(group);
                    }

                    group.amount += investment.amount;

                    return groupedInvestments;
                },
                [],
            ),
            ['label']
        );
    }

    getInvestmentsDataByMonth = () => {
        const initialGroupedInvestments = [...new Array(12)].map(
            (item, index) => {
                const month = index + 1;
                const paddedMonth = String(month).padStart(2, 0);

                return {
                    label: moment(`1970-${paddedMonth}-01`).format('MMM'),
                    month,
                    amount: 0,
                };
            }
        );

        return sortBy(
            this.getInvestments().reduce(
                (groupedInvestments, investment) => {
                    const investmentMonth = Number(moment(investment.dateTime).format('M'));
                    const group = groupedInvestments.find(group => group.month === investmentMonth);

                    group.amount += investment.amount;

                    return groupedInvestments;
                },
                initialGroupedInvestments,
            ),
            ['month']
        );
    }

    componentDidUpdate() {
        const subBudgetsData = orderBy(
            this.getSubBudgetsData(),
            ['amount', 'label'],
            ['desc', 'asc']
        );

        this.investmentsCategoryChartObject.data.labels = subBudgetsData.map(datum => datum.label),
        this.investmentsCategoryChartObject.data.datasets = [{
            data: subBudgetsData.map(datum => datum.amount.toFixed(0)),
            backgroundColor: Object.keys(subBudgetsData).map(
                index => `rgba(0, 0, 0, ${ 0.1 + index /(subBudgetsData.length + 2) })`
            ),
            borderWidth: 0,
        }];

        const investmentsData = this.getInvestmentData();
        const totalInvestmentsAmount = investmentsData.reduce(
            (total, datum) => total += datum.amount,
            0,
        );
        const maxInvestmentAmout = investmentsData.length  > 0 && maxBy(investmentsData, 'amount').amount;

        this.investmentsMonthYearChartObject.data.labels = investmentsData.map(datum => datum.label);
        this.investmentsMonthYearChartObject.data.datasets = [{
            data: investmentsData.map(datum => datum.amount),
            backgroundColor: investmentsData.map(() => 'rgba(0, 0, 0, 0.5)'),
        }];
        this.investmentsMonthYearChartObject.options.scales = {
            xAxes: [{
                gridLines: {
                    drawOnChartArea: false
                }
            }],
            yAxes: [{
                gridLines: {
                    drawOnChartArea: false
                },
                ticks: {
                    suggestedMax: maxInvestmentAmout * 1.2,
                },
            }]
        },

        this.investmentsMonthYearChartObject.options.animation =  { onProgress: function() {
            let chartInstance = this.chart,
            ctx = chartInstance.ctx;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';

            this.data.datasets.forEach(function (dataset, i) {
                let meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function (bar, index) {
                    var data = dataset.data[index];

                    if (data > 0) {
                        ctx.font = '12px Helvetica';
                        ctx.fillStyle = "#2f3036";
                        ctx.fillText(`${(data).toFixed(0)} €`, bar._model.x, bar._model.y - 15);

                        ctx.font = '11px Helvetica';
                        ctx.fillStyle = "#9b9b9b";
                        ctx.fillText(`${(data * 100 / totalInvestmentsAmount).toFixed()} %`, bar._model.x, bar._model.y - 2);
                    }
                });
            });
        }}

        this.investmentsCategoryChartObject.update();
        this.investmentsMonthYearChartObject.update();

        return true;
    }

    componentDidMount() {
        const subBudgetsData = orderBy(
            this.getSubBudgetsData(),
            ['amount', 'label'],
            ['desc', 'asc']
        );
        this.investmentsCategoryChartObject = new Chart(this.investmentsCategoryChart.current, {
            type: 'pie',
            tooltips: {
                enabled: false,
            },
            data: {
                labels: subBudgetsData.map(datum => datum.label),
                datasets: [{
                    label: '# of Votes',
                    data: subBudgetsData.map(datum => datum.amount.toFixed(0)),
                    backgroundColor: Object.keys(subBudgetsData).map(
                        index => `rgba(0, 0, 0, ${ 0.1 + index /(subBudgetsData.length + 2) })`
                    ),
                    borderWidth: 0,
                }]
            },
            options: {
                legend: {
                    display: false,
                },
            },
        });

        const investmentsData = this.getInvestmentData();
        const maxInvestmentAmout = investmentsData.length  > 0 && maxBy(investmentsData, 'amount').amount;
        const totalInvestmentsAmount = investmentsData.reduce(
            (total, datum) => total += datum.amount,
            0,
        );

        this.investmentsMonthYearChartObject = new Chart(this.investmentsMonthYearChart.current, {
            type: 'bar',
            data: {
                labels: investmentsData.map(datum => datum.label),
                datasets: [{
                    label: '# of Votes',
                    data: investmentsData.map(datum => datum.amount),
                    backgroundColor: investmentsData.map(() => 'rgba(0, 0, 0, 0.5)'),
                    borderWidth: 0,
                }]
            },
            options: {
                legend: {
                    display: false,
                },
                tooltips: {
                    enabled: false,
                },
                hover: {
                    mode: false,
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            drawOnChartArea: false
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            drawOnChartArea: false
                        },
                        ticks: {
                            suggestedMax: maxInvestmentAmout * 1.2,
                        },
                    }]
                },
                animation: {
                    onProgress: function() {
                        let chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset, i) {
                            let meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];

                                if (data > 0) {
                                    ctx.font = '12px Helvetica';
                                    ctx.fillStyle = "#2f3036";
                                    ctx.fillText(`${(data).toFixed(0)} €`, bar._model.x, bar._model.y - 15);

                                    ctx.font = '11px Helvetica';
                                    ctx.fillStyle = "#9b9b9b";
                                    ctx.fillText(`${(data * 100 / totalInvestmentsAmount).toFixed()} %`, bar._model.x, bar._model.y - 2);
                                }
                            });
                        });
                    },
                },
            },
        });
    }

    render() {
        const totalInvestmentsAmount = this.getInvestments().reduce(
            (total, investment) => total += investment.amount,
            0,
        ).toFixed(0);

        const budgetCategories = orderBy(
            this.props.subBudgets,
            ['amount', 'name'],
            ['desc', 'asc']
        );

        return (
            <Card className="investments" contentClassName="investments-content" >
                <div className="investment-section">
                    <Text.Medium type={Text.types.PRIMARY} size={Text.sizes.REGULAR} transform={Text.transform.UPPERCASE} >
                        {Strings['invest.by.budget.category']}
                    </Text.Medium>
                    <div className="investment-section__data is-flex">
                        <canvas
                            id="investments-category-chart"
                            ref={this.investmentsCategoryChart}
                            width="200"
                            height="200"
                        ></canvas>
                        <ul className="investment-services__list">
                            {budgetCategories.map((subBudget, index) =>
                                subBudget.amount > 0 &&
                                <li key={subBudget.identifier} onClick={() => this.props.onSubBudgetClick(subBudget.name)}>
                                    <div>
                                        <Icon name="Input" className="investment-services-icon"/>
                                        <span style={{background: `rgba(0, 0, 0, ${ 0.1 + index / (budgetCategories.length + 2) })`}} />
                                        <em>{ (subBudget.amount.toFixed(2) * 100 / totalInvestmentsAmount).toFixed() }%</em>{ subBudget.name }
                                    </div>
                                    <span>{`${ formatSum(subBudget.amount.toFixed(0)) } `}</span>
                                </li>
                            ) }
                            <li>{Strings['total']}
                                <strong>
                                {`${ formatSum(totalInvestmentsAmount) } `}
                                </strong>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="investment-section investment-section--double">
                    <div>
                        <Text.Medium type={Text.types.PRIMARY} size={Text.sizes.REGULAR} transform={Text.transform.UPPERCASE} >
                            { this.props.period === 'all' ? Strings['invest.by.year'] : Strings['invest.by.month'] }
                        </Text.Medium>
                        <div className="investment-section__data is-flex">
                            <canvas
                                id="investments-year-chart"
                                ref={this.investmentsMonthYearChart}
                                width="400"
                                height="200"
                            ></canvas>
                        </div>
                    </div>
                    <div>
                        <Text.Medium type={Text.types.PRIMARY} size={Text.sizes.REGULAR} transform={Text.transform.UPPERCASE} >
                                {Strings['processes.after.service']}
                        </Text.Medium>
                        <div className="investment-section__data">
                            <ul className="investment-services__list investment-services__list--simple">
                                {
                                    this.getServices().map(service => (
                                        service.investments.length > 0 &&
                                        <li key={service.identifier}>
                                            { service.title } <strong>{ service.investments.length }</strong>
                                        </li>
                                    ))
                                }
                                <li>
                                    {Strings['total.number']}
                                    <strong>
                                        {this.getInvestments().length }
                                    </strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </Card>
        );
    }
}
