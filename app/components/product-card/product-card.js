import React from 'react';
import AuthenticatedImage from '../common/authenticated-image/authenticated-image';
import cokeLogo from '../../../assets/cokeLogo.jpg';
import './product-card.css';

const Image = ({ imageUrl }) => {
    const fullUrl = imageUrl && imageUrl.match('http');

    return (
        fullUrl ? (
            <img alt="" src={imageUrl || cokeLogo} />
        ) : (
            <AuthenticatedImage alt="" src={imageUrl ? `/api/documentFile/${imageUrl}` : cokeLogo} />
        )
    );
};

const ProductCard = ({
    onClick,
    imageId,
    className,
}) => (
    <div className={`product-card ${className || ''}`} onClick={onClick}>
        <Image imageUrl={imageId || ''} />
    </div>
);

export default ProductCard;
