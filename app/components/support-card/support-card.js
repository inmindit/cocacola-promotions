import React from 'react';
import PropTypes from 'prop-types';

import { Card, Button, Icon, Text } from 'components/common';
import { Strings } from 'helpers';
import image from './outline-build.svg';
import './support-card.css';

const SupportCard = ({ email, telephone, children }) => (
    <Card contentClassName="support-card-content">
        <div className="support-card-title">
            <Icon className="support-icon" name="ContactSupportOutlined" />
            <Text.Bold size={Text.sizes.MEDIUM}>{Strings['support.title']}</Text.Bold>
        </div>
        <div className="support-card-info">
            <Button.Outlined className="support-button">
                <a className="mail-link" href={`mailto:${email}`} >
                    <img className="support-card-image" src={image} alt="" />
                    <Text className="support-button-text" type={Text.types.NONE} transform={Text.transform.UPPERCASE}>
                        {Strings['support.serviceTask']}
                    </Text>
                </a>
            </Button.Outlined>
            <Text className="support-text" type={Text.types.SECONDARY} size={Text.sizes.SMALL}>
                {Strings['support.info.first.row']}
                {'\n'}
                {Strings['support.info.second.row']}
            </Text>
            <Text className="support-text" type={Text.types.SECONDARY} size={Text.sizes.SMALL}>
                {Strings['support.telephone.first.row']}
                {'\n'}
                {`${Strings['support.telephone.second.row']} ${telephone}...`}
            </Text>
        </div>
        {children}
    </Card>
);

SupportCard.propTypes = {
    email: PropTypes.string.isRequired,
    telephone: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

SupportCard.defaultProps = {
    children: null,
};

export default SupportCard;
