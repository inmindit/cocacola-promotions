import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Card, Icon, ImageCircle, ProgressBar, Text } from 'components/common';
import { Strings, formatSum } from 'helpers';

import './budget-card.css';

const BudgetCard = ({
    className, title, period, budgetUsed, budgetLimit, id, selected, titleImgUrl, onClick,
}) => (
    <Card
        className={`budget-container ${className}`}
        contentClassName="budget-card"
        selected={selected}
        onClick={() => {
            onClick(id);
        }}
    >
        <div className="budget-card-header">
            <ImageCircle className="budget-badge" imageUrl={titleImgUrl} />
            <Text>{title}</Text>
            {period && <span className="budget-period">{period}</span>}
        </div>
        <div className="budget-progress-container">
            {budgetLimit ? (
                <Fragment>
                    <ProgressBar className="budget-progress" amount={budgetUsed} total={budgetLimit} />
                    <div className="flex-between">
                        <Text className="budget-percentage">{`${(budgetUsed / budgetLimit) * 100}%`}</Text>
                        <Text.Bold className="budget-amount">{`${formatSum(budgetUsed)}`}</Text.Bold>
                    </div>
                    <div className="flex-between">
                        <Text type={Text.types.SECONDARY} size={Text.sizes.SMALL}>
                            {Strings['budget.card.percentage']}
                        </Text>
                        <Text type={Text.types.SECONDARY} size={Text.sizes.SMALL} className="budget-total">
                            {`${Strings['budget.from']} ${formatSum(budgetLimit)}`}
                        </Text>
                    </div>
                </Fragment>
            ) : (
                <div className="budget-info-container">
                    <Text.Bold className="budget-amount-text">{`${formatSum(budgetUsed)}`}</Text.Bold>
                    <div className="budget-link">
                        <div>{Strings['budget.click']}</div>
                        <Icon name={selected ? 'ChevronLeft' : 'ChevronRight'} />
                    </div>
                </div>
            )}
        </div>
    </Card>
);

BudgetCard.defaultProps = {
    className: '',
    titleImgUrl: '',
    url: '',
};

export default BudgetCard;
