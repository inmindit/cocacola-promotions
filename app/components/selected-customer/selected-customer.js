import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { CustomerInfoModal } from 'components';
import { Icon } from 'components/common';
import { Link } from 'react-router-dom';
import { Strings } from 'helpers';

import './selected-customer.css';

const CustomerDetail = ({
    className, label, value, url, showArrow, children, onClick,
}) => {
    const content = (
        <Fragment>
            <div className="info">
                <span className="label">{Strings[label]}</span>
                <span className="value">{value}</span>
            </div>
            <div className="right-node">
                {showArrow && <Icon className="arrow-down" name="KeyboardArrowDown" />}
                {children}
            </div>
        </Fragment>
    );

    return (
        <div className={`customer-detail ${className}`} onClick={onClick}>
            {url ? (
                <Link className="customer-link" to={url}>
                    {content}
                </Link>
            ) : (
                content
            )}
        </div>
    );
};

CustomerDetail.propTypes = {
    className: PropTypes.string,
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
    url: PropTypes.string,
    onClick: PropTypes.func,
};
CustomerDetail.defaultProps = {
    className: '',
    onClick: null,
    url: '',
};

const SelectedCustomer = ({
    className,
    name,
    customerId,
    classification,
    stc,
    onDeleteSelectedCustomerClick,
    onCustomerNameClick,
    showCustomerInfo,
    selectedCustomer,
    onCustomerInfoModalClose,
}) => (
    <div className={`customer-details-container ${className}`}>
        <CustomerDetail showArrow className="customer-name" label="customer.name" value={name} onClick={onCustomerNameClick}>
            {
                showCustomerInfo && (
                    <CustomerInfoModal
                        className="customer-info-dashboard"
                        data={selectedCustomer}
                        hasData={!!selectedCustomer}
                        onCloseClick={onCustomerInfoModalClose}
                    />
                )
            }

        </CustomerDetail>
        <CustomerDetail label="customer.sapNumber" value={customerId} />
        <CustomerDetail label="customer.stcCode" value={`${stc.code} - ${stc.displayName}`} />
        <CustomerDetail label="customer.classification" value={Strings[`customer.classification.${classification}`] || classification} />
        <div className="close-icon">
            <Icon name="Close" onClick={onDeleteSelectedCustomerClick} />
        </div>
    </div>
);

SelectedCustomer.propTypes = {
    className: PropTypes.string,
    name: PropTypes.string.isRequired,
    customerId: PropTypes.string.isRequired,
    classification: PropTypes.string.isRequired,
    stc: PropTypes.object.isRequired,
    onDeleteSelectedCustomerClick: PropTypes.func.isRequired,
    onCustomerNameClick: PropTypes.func.isRequired,

};

SelectedCustomer.defaultProps = {
    className: '',
};

export default SelectedCustomer;
