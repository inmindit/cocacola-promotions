import { ProductsService } from 'services';

export const getProducts = () => dispatch => {
    dispatch({ type: 'GET_PRODUCTS_REQUEST' });

    ProductsService.getProducts()
        .then(
            ({ results: products }) => {
                dispatch({ type: 'SET_PRODUCTS_DATA', products });
            },
            error => {
                console.log('Products error', error);
            },
        )
        .finally(() => {
            dispatch({ type: 'GET_PRODUCTS_REQUEST_END' });
        });
};

export const getProductData = id => dispatch => {
    dispatch({ type: 'GET_PRODUCT_REQUEST' });

    ProductsService.getProductData({ urlParams: { id } })
        .then(
            ({ results: product }) => {
                dispatch({ type: 'SET_PRODUCT_DATA', product });
            },
            error => {
                console.log('Products error', error);
            },
        )
        .finally(() => {
            dispatch({ type: 'GET_PRODUCT_REQUEST_END' });
        });
};

export const updateSelectedProduct = id => ({
    type: 'UPDATE_SELECTED_PRODUCT',
    id,
});
