import { BudgetService } from 'services';

// Used for customers without customerClass!!!
const DEFAULT_BUDGET_OBJECT = {
    customerClass: "",
    mainBudgetCategories: [],
    sapId: "",
};

export const getBudgetData = sapId => dispatch => {
    dispatch({ type: 'GET_BUDGET_REQUEST' });

    BudgetService.getBudget({ urlParams: { sapId } })
        .then(
            budgetData => {
                dispatch({ type: 'SET_BUDGET_DATA', budgetData: Object.keys(budgetData).length ? budgetData : DEFAULT_BUDGET_OBJECT });
            },
            error => {
                console.log('Budget error', error);
            },
        )
        .finally(() => {
            dispatch({ type: 'GET_BUDGET_REQUEST_END' });
        });
};

export const updateSelectedBudget = identifier => ({
    type: 'UPDATE_SELECTED_BUDGET',
    identifier,
});
