import { ToolsService } from 'services';

const parseToolRequestData = ({ selectedCustomer, selectedProduct, userData, }) => ({
    brandId: selectedProduct.id || '',
    packaging: '',
    stcId: selectedCustomer.stc.code,
    classification: selectedCustomer.classification,
    redScore: '',
    budget: '',
    orderType: '',
    zterm: selectedCustomer.zterm,
    ztermSp: selectedCustomer.ztermSp,
    directCustomer: selectedCustomer.directCustomer,
});

export const getTools = ({ selectedCustomer, selectedProduct, userData }) => dispatch => {
    const urlParams = parseToolRequestData({ selectedCustomer, selectedProduct, userData });

    dispatch({ type: 'GET_TOOLS_REQUEST' });

    ToolsService.getTools({ urlParams })
        .then(
            tools => {
                dispatch({ type: 'SET_TOOLS_DATA', tools });
            },
            error => {
                console.log('Tools error', error);
            },
        )
        .finally(() => {
            dispatch({ type: 'GET_TOOLS_REQUEST_END' });
        });
};

export const updateSelectedTool = identifier => ({
    type: 'UPDATE_SELECTED_TOOL',
    identifier,
});

export const updateToolsFilter = activeFilter => ({
    type: 'UPDATE_TOOLS_FILTER',
    activeFilter,
});
