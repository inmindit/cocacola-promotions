import { CustomersService } from 'services';

export const getCustomerData = id => dispatch => {
    dispatch({ type: 'GET_CUSTOMER_REQUEST' });

    CustomersService.getCustomerData({ urlParams: { id } })
        .then(
            customer => {
                dispatch({ type: 'SET_CUSTOMER_DATA', customer });
            },
            error => {
                console.log('Customers error', error);
            },
        )
        .finally(() => {
            dispatch({ type: 'GET_CUSTOMER_REQUEST_END' });
        });
};

