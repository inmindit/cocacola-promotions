export const setPlatformType = platformType => ({
    type: 'SET_PLATFORM_TYPE',
    platformType,
});

export const setActiveView = viewKey => ({
    type: 'SET_ACTIVE_VIEW',
    viewKey,
});

export const updateWelcomeMessageCloseTimestamp = () => dispatch => {
    const welcomeMessageCloseTimestamp = +new Date();

    localStorage.setItem('welcomeMessageCloseTimestamp', welcomeMessageCloseTimestamp);

    dispatch({
        type: 'SET_WELCOME_MESSAGE_CLOSE_TIMESTAMP',
        welcomeMessageCloseTimestamp,
    });
};

export const setLanguage = lang => ({
    type: 'SET_LANGUAGE',
    lang,
});
