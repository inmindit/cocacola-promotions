import { UserService } from 'services';
import { setItemInLocalStorage } from 'helpers';
import { setLanguage } from './application';

export const setAccessToken = accessToken => ({
    type: 'SET_ACCESS_TOKEN',
    accessToken,
});

export const getAccessToken = authCode => dispatch => {
    dispatch({ type: 'GET_ACCESS_TOKEN_REQUEST' });

    const data = { authCode, redirectUrl: `${window.location.origin}/login/oauth` };

    UserService.getAccessToken({ data })
        .then(
            ({ accessToken }) => {
                dispatch({ type: 'SET_ACCESS_TOKEN_DATA', accessToken });
                setItemInLocalStorage('COKE_AGENT.accessToken', accessToken);
                setItemInLocalStorage('WelcomeMessegeShow', true);
            },
            error => {
                console.error('Authentication error: ', error);
            },
        )
        .finally(() => dispatch({ type: 'GET_ACCESS_TOKEN_REQUEST_END' }));
};

export const getUserData = () => dispatch => {
    dispatch({ type: 'GET_USER_DATA' });

    UserService.getUserData()
        .then(
            userData => {
                dispatch({ type: 'SET_USER_DATA', userData });
                dispatch(setLanguage(userData.language));
            },
            error => {
                console.error('Authentication error: ', error);
            },
        )
        .finally(() => dispatch({ type: 'SET_USER_DATA_REQUEST_END' }));
};

export const getUserWelcomeMessage = () => dispatch => {
    dispatch({ type: 'GET_USER_WELCOME_MESSAGE' });

    UserService.getUserWelcomeMessage()
        .then(
            welcomeMessage => {
                dispatch({ type: 'SET_USER_WELCOME_MESSAGE', welcomeMessage });
            },
            error => {
                console.error('Authentication error: ', error);
            },
        )
        .finally(() => dispatch({ type: 'SET_USER_WELCOME_MESSAGE_REQUEST_END' }));
}

export const logout = () => dispatch => {
    dispatch({ type: 'LOGOUT_REQUEST' });

    UserService.logout()
        .then(
            () => {
                localStorage.removeItem('COKE_AGENT.accessToken');
                localStorage.removeItem('WelcomeMessegeShow');
                window.location.assign('/logout-redirect');
            },
            error => {
                console.error('Logout error: ', error);
            },
        )
        .finally(() => dispatch({ type: 'LOGOUT_REQUEST_END' }));
};
