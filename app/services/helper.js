import axios from 'axios';
import { parse, stringify } from 'qs';
import JSONHeaders from './headers';

const instance = axios.create({
    baseURL: '/api',
    timeout: 20000,
});

export const getAxiosInstance = () => instance;

export const serviceBind = ({
    authorized = true,
    headers = { ...JSONHeaders },
    url,
    ...rest
}) => ({
    params,
    data,
    urlParams = {},
    pathname = '',
} = {}) => new Promise((resolve, reject) => {
    if (authorized) {
        const token = localStorage.getItem('COKE_AGENT.accessToken');

        headers.Authorization = `Token ${token}`;
    }

    const baseURL = urlParams && typeof url !== 'string' ? url(urlParams) : url;
    const request = {
        ...rest,
        headers,
        url: `${baseURL}${pathname}`,
        params,
        data,
    };

    instance.request(request).then(({ data }) => {
        resolve(data);
    }, error => {
        if (error.response && error.response.status === 401) {
            localStorage.removeItem('COKE_AGENT.accessToken');

            const redirectQuery = {
                scope: 'all',
                redirect_uri: `${window.location.origin}/login/oauth`,
                response_type: 'code',
                client_id: 'cce-coke-agent',
                ...parse(window.location.search, {
                    ignoreQueryPrefix: true,
                }),
            };

            window.location.assign(`/login-redirect?${stringify(redirectQuery)}`);
        }

        reject(error);
    });
});

export const createServices = services => services.reduce((currentServices, { name, ...service }) => ({
    ...currentServices,
    [name]: serviceBind(service),
}), {});

export const mockService = data => () => new Promise(resolve => setTimeout(() => resolve(data), 500));
