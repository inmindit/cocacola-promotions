import { createServices } from './helper';

const services = [
    {
        name: 'getProducts',
        url: '/brands',
        method: 'GET',
    },
    {
        name: 'getProductData',
        url: ({ id }) => `/brands?query=${id}`,
        method: 'GET',
    },
];

export default createServices(services);
