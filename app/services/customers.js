import { createServices } from './helper';

const services = [
    {
        name: 'getCustomers',
        url: '/customers',
        method: 'GET',
    },
    {
        name: 'getCustomerData',
        url: ({ id }) => `/customers/${id}`,
        method: 'GET',
    },
];

export default createServices(services);
