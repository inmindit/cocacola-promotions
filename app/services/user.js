import { createServices } from './helper';

const services = [
    {
        name: 'getAccessToken',
        url: '/login/oauth',
        method: 'POST',
    },
    {
        name: 'getUserData',
        url: '/userData',
        method: 'GET',
    },
    {
        name: 'logout',
        url: '/login',
        method: 'DELETE',
    },
    {
        name: 'getUserWelcomeMessage',
        url: '/welcomeMessage',
        method: 'GET',
    }
];

export default createServices(services);
