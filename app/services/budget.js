import { createServices } from './helper';

const services = [
    {
        name: 'getBudget',
        url: ({ sapId }) => `/budget?sapNumber=${sapId}`,
        method: 'GET',
    },
];

export default createServices(services);
