import { createServices } from './helper';

const services = [
    {
        name: 'getTools',
        url: ({
            brandId = '',
            packaging = '',
            stcId = '',
            classification = '',
            redScore = '',
            budget = '',
            orderType = '',
            zterm = '',
            ztermSp = '',
            directCustomer = '',
        }) => `/qualification?brandId=${brandId}&packaging=${packaging}&stcId=${stcId}
            &customerClassification=${classification}&redScore=${redScore}&budget=${budget}
            &orderType=${orderType}&zterm=${zterm}&ztermSp=${ztermSp}&directCustomer=${directCustomer}`,
        method: 'GET',
        authorized: true,
    },
];

export default createServices(services);
