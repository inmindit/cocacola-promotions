import { reducer } from 'helpers';
import { pick } from 'lodash';

const getInvestments = budget => {
    const investments = [];

    budget.subBudgetCategories.forEach(sbc =>
        sbc.services.forEach(service => {
            investments.push(...service.investments);
        }));

    return investments;
};

const parseBudgets = budgetData => {
    const data = budgetData && budgetData.budgets.map(budget => ({
        ...pick(budget, ['budgetLimit', 'budgetUsed', 'identifier', 'name']),
        investments: getInvestments(budget),
    }));
    const customerBudget = data.reduce((acc, val) => acc + (val.budgetLimit - val.budgetUsed), 0);

    return {
        data,
        customerBudget,
    };
};

export default reducer(
    {
        customerBudget: 0,
        isLoading: false,
        budgetData: [],
        selectedSubBudget: {},
    },
    {
        GET_BUDGET_REQUEST: state => ({
            ...state,
            isLoading: true,
        }),
        SET_BUDGET_DATA: (state, { budgetData }) => ({
            ...state,
            budgetData: budgetData.mainBudgetCategories,
        }),
        GET_BUDGET_REQUEST_END: state => ({
            ...state,
            isLoading: false,
        }),
        UPDATE_SELECTED_BUDGET: (state, { identifier }) => ({
            ...state,
            selectedSubBudget: state.budgetData.flatMap(budget => budget.subBudgetCategories.filter(subBudget => (subBudget.identifier === identifier))),
        }),
    },
);
