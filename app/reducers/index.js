import { combineReducers } from 'redux';
import application from './application';
import user from './user';
import products from './products';
import customers from './customers';
import tools from './tools';
import budget from './budget';

export default combineReducers({
    application,
    user,
    products,
    customers,
    tools,
    budget,
});
