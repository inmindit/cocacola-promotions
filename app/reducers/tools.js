import { reducer } from 'helpers';

export default reducer(
    {
        tools: [],
        isLoading: false,
        selectedTool: {},
        isSelectedToolLoading: false,
        activeFilter: 'normal',
    },
    {
        GET_TOOLS_REQUEST: state => ({
            ...state,
            isLoading: true,
        }),
        SET_TOOLS_DATA: (state, { tools }) => ({
            ...state,
            tools,
        }),
        GET_TOOLS_REQUEST_END: state => ({
            ...state,
            isLoading: false,
        }),
        UPDATE_SELECTED_TOOL: (state, { identifier }) => ({
            ...state,
            selectedTool: state.tools.filter(tool => tool.identifier === identifier)[0] || {},
        }),
        GET_TOOL_REQUEST: state => ({
            ...state,
            isSelectedToolLoading: true,
        }),
        SET_TOOL_DATA: (state, { tool }) => ({
            ...state,
            selectedTool: {
                ...tool,
                imageUrl: tool.url,
            },
        }),
        GET_TOOL_REQUEST_END: state => ({
            ...state,
            isSelectedToolLoading: false,
        }),
        UPDATE_TOOLS_FILTER: (state, { activeFilter }) => ({
            ...state,
            activeFilter,
        }),
    },
);
