import { reducer } from 'helpers';

export default reducer(
    {
        customers: [],
        isLoadingCustomers: false,
        isLoadingCustomerData: false,
        selectedCustomer: { id: null },
    },
    {
        GET_CUSTOMERS_REQUEST: state => ({
            ...state,
            isLoadingCustomers: true,
        }),
        GET_CUSTOMER_REQUEST: state => ({
            ...state,
            isLoadingCustomerData: true,
        }),
        SET_CUSTOMER_DATA: (state, { customer }) => ({
            ...state,
            selectedCustomer: customer,
        }),
        GET_CUSTOMER_REQUEST_END: state => ({
            ...state,
            isLoadingCustomerData: false,
        }),
    },
);
