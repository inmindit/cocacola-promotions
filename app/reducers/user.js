import { reducer } from 'helpers';

export default reducer(
    {
        isAccessTokenLoaded: false,
        accessToken: null,
        userData: {},
    },
    {
        GET_ACCESS_TOKEN_REQUEST_END: state => ({
            ...state,
            isAccessTokenLoaded: true,
        }),
        SET_ACCESS_TOKEN: (state, { accessToken }) => ({
            ...state,
            accessToken,
            isAccessTokenLoaded: true,
        }),
        SET_ACCESS_TOKEN_DATA: (state, { accessToken }) => ({
            ...state,
            accessToken,
        }),
        SET_USER_DATA: (state, { userData }) => {
            const nameParts = userData.fullName.split(' ');
            const shortUsername = `${nameParts[0][0].toUpperCase()}${nameParts[nameParts.length - 1][0].toUpperCase()}`;

            return {
                ...state,
                userData: {
                    ...userData,
                    shortUsername,
                },
            };
        },
        SET_USER_WELCOME_MESSAGE: (state, { welcomeMessage }) => ({
            ...state,
            welcomeMessage,
        }),
    },
);
