import { parse } from 'qs';
import { reducer } from 'helpers';

const { brands } = parse(window.location.search, { ignoreQueryPrefix: true });

export default reducer(
    {
        products: [],
        isLoading: false,
        selectedProduct: { id: brands || null },
    },
    {
        GET_PRODUCTS_REQUEST: state => ({
            ...state,
            isLoading: true,
        }),
        SET_PRODUCTS_DATA: (state, { products }) => ({
            ...state,
            products,
            selectedProduct: state.selectedProduct.id ? products.find(p => p.id === state.selectedProduct.id) : { id: null },
        }),
        GET_PRODUCTS_REQUEST_END: state => ({
            ...state,
            isLoading: false,
        }),
        UPDATE_SELECTED_PRODUCT: (state, { id }) => ({
            ...state,
            selectedProduct: state.products.filter(product => product.id === id)[0] || {},
        }),
    },
);
