import { persistReducer } from 'redux-persist';
import storage from "redux-persist/lib/storage";
import { reducer, Constants, getPlatformType, getItemFromLocalStorage } from 'helpers';

const activeViewMapping = {
    normal: {
        key: 'normal',
        titleKey: 'page.dashboard.title',
        showNavigation: true,
    },
    reporting: {
        key: 'reporting',
        titleKey: 'page.statistics.title',
        showNavigation: true,
    },
    products: {
        key: 'products',
        titleKey: 'page.selectProduct.title',
        showNavigation: false,
    },
    selectCustomer: {
        key: 'selectCustomer',
        titleKey: 'page.selectCustomer.title',
        showNavigation: true,
    },
    toolDetails: {
        key: 'toolDetails',
        titleKey: 'page.toolDetails.title',
        showNavigation: true,
    },
    budget: {
        key: 'budget',
        titleKey: 'page.budget.title',
        showNavigation: false,
    },

};


const ApplicationReducer = reducer(
    {
        platformType: getPlatformType(),
        lang: Constants.DefaultSettings.lang,
        activeView: activeViewMapping.normal,
        showNavigation: activeViewMapping.normal.showNavigation,
        isLoading: false,
        welcomeMessageCloseTimestamp: localStorage.getItem('welcomeMessageCloseTimestamp') || '631152000000', // Default to 1990 ( random year in the past )
    },
    {
        SET_PLATFORM_TYPE: (state, { platformType }) => ({
            ...state,
            platformType,
        }),
        SET_LANGUAGE: (state, { lang }) => ({
            ...state,
            lang,
        }),
        SET_ACTIVE_VIEW: (state, { viewKey }) => ({
            ...state,
            activeView: activeViewMapping[viewKey],
            showNavigation: activeViewMapping[viewKey].showNavigation,
        }),
        SET_WELCOME_MESSAGE_CLOSE_TIMESTAMP: (state, { welcomeMessageCloseTimestamp }) => ({
            ...state,
            welcomeMessageCloseTimestamp,
        }),
    },
);

const persistConfig = {
    key: 'application',
    storage,
    whitelist: ['lang'],
};

export default persistReducer(persistConfig, ApplicationReducer);
