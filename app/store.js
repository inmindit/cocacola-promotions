import { createStore as createReduxStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from "redux-persist/lib/storage";

import rootReducer from './reducers';

const persistConfig = {
    key: 'root',
    storage,
    whitelist: [],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const createStore = (initialState = {}) => {
    const store = createReduxStore(persistedReducer, initialState, compose(applyMiddleware(thunk)));

    const persistor = persistStore(store);

    return { store, persistor };
};
