const url = require('url');
const path = require('path');
const webpack = require('webpack');
const { name } = require('./package.json');
const WebpackMd5Hash = require('webpack-md5-hash');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const isDev = process.env.NODE_ENV !== 'production';

const NODE_PORT = process.env.NODE_PORT || 3000;
const API_URL = process.env.API_URL || 'https://stage-core.cce-cloud.de/coke-agent-rest/api';
const LOGIN_REDIRECT = process.env.LOGIN_REDIRECT || 'https://stage-core.cce-cloud.de/authenticate';
const LOGOUT_REDIRECT =
    process.env.LOGOUT_REDIRECT ||
    'https://stage-core.cce-cloud.de/logout?scope=all&redirect_uri=http://localhost:3000/login/oauth&response_type=code&client_id=cce-coke-agent';
const DIST_PATH = path.resolve(__dirname, 'dist');

const config = {
    entry: { main: ['babel-polyfill', './app/index.js'] },
    output: {
        path: DIST_PATH,
        filename: `${name}.[chunkhash].js`,
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /\.css$/,
                use: [
                    isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { importLoaders: 1, sourceMap: true } },
                    { loader: 'postcss-loader', options: { sourceMap: true, config: { path: path.resolve(__dirname, 'postcss.config.js') } } },
                ],
            },
            {
                test: /\.(eot|png||pdf|jpg|svg|ttf|otf|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]',
                },
            },
        ],
    },
    plugins: [
        new WebpackMd5Hash(),
        new webpack.NamedModulesPlugin(),
        new MiniCssExtractPlugin({
            filename: `${name}.[contenthash].css`,
        }),
        new HtmlWebpackPlugin({
            template: './app/index.html',
            filename: 'index.html',
        }),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: { discardComments: { removeAll: true } },
            canPrint: true,
        }),
        new CopyWebpackPlugin([{ from: path.resolve(__dirname, 'l10n'), to: 'l10n' }]),
    ],
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: 2,
                    name: 'vendors',
                    chunks: 'all',
                },
            },
        },
    },
    devServer: {
        contentBase: DIST_PATH,
        compress: true,
        open: true,
        port: NODE_PORT,
        historyApiFallback: true,
        setup: app => {
            app.get('/login-redirect', (req, res) => {
                const redirect = url.format({
                    pathname: LOGIN_REDIRECT,
                    query: req.query,
                });

                return res.redirect(redirect);
            });

            app.get('/logout-redirect', (req, res) => {
                return res.redirect(LOGOUT_REDIRECT);
            });
        },
        proxy: {
            '/api': {
                target: `${API_URL}`,
                changeOrigin: true,
                secure: true,
                pathRewrite: { '^/api': '' },
            },
        },
    },
    resolve: {
        extensions: ['*', '.js', '.jsx', '.css'],
        alias: {
            actions: path.resolve(__dirname, 'app/actions'),
            reducers: path.resolve(__dirname, 'app/reducers'),
            containers: path.resolve(__dirname, 'app/containers'),
            constants: path.resolve(__dirname, 'app/helpers/constants.js'),
            components: path.resolve(__dirname, 'app/components'),
            services: path.resolve(__dirname, 'app/services'),
            helpers: path.resolve(__dirname, 'app/helpers'),
            hoc: path.resolve(__dirname, 'app/hoc'),
            root: path.resolve(__dirname, 'app'),
        },
    },
};

if (isDev) {
    config.devtool = 'eval-source-map';
}

module.exports = config;
